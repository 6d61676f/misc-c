#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdlib.h>
int
main(int argc, char* argv[])
{

  if (SDL_Init(SDL_INIT_VIDEO) < 0) {
    printf("Eroare init: %s\n", SDL_GetError());
    return (EXIT_FAILURE);
  } else {
    SDL_Window* fereastra = SDL_CreateWindow("Hello",
                                             SDL_WINDOWPOS_UNDEFINED,
                                             SDL_WINDOWPOS_UNDEFINED,
                                             640,
                                             480,
                                             SDL_WINDOW_SHOWN);
    if (fereastra == 0) {
      printf("Eroare la creare fereastra %s\n", SDL_GetError());
      return (EXIT_FAILURE);
    } else {
      SDL_Surface* surf = SDL_GetWindowSurface(fereastra);
      SDL_FillRect(surf, 0, SDL_MapRGB(surf->format, 0xFF, 0xFF, 0xFF));
      SDL_UpdateWindowSurface(fereastra);
      SDL_Delay(2000);
      SDL_DestroyWindow(fereastra);
      SDL_Quit();
      return (EXIT_SUCCESS);
    }
  }

  return (EXIT_SUCCESS);
}
