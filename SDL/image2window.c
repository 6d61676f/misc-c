#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdlib.h>
int
main(int argc, char* argv[])
{
  if (SDL_Init(SDL_INIT_VIDEO) < 0) {
    printf("SDL_Init problem: %s\n", SDL_GetError());
    return (EXIT_FAILURE);
  } else {
    SDL_Window* window = SDL_CreateWindow("Image",
                                          SDL_WINDOWPOS_UNDEFINED,
                                          SDL_WINDOWPOS_UNDEFINED,
                                          640,
                                          480,
                                          SDL_WINDOW_SHOWN);
    if (!window) {
      printf("SDL_CreateWindow problem: %s\n", SDL_GetError());
      return (EXIT_FAILURE);
    } else {
      SDL_Surface* surface = SDL_GetWindowSurface(window);
      SDL_Surface* image = SDL_LoadBMP("image.bmp");
      if (!surface || !image) {
        printf("image or surface null: %s\n", SDL_GetError());
        if (!image) {
          SDL_FreeSurface(surface);
        }
        SDL_DestroyWindow(window);
        return (EXIT_FAILURE);
      } else {
        SDL_BlitSurface(image, 0, surface, 0);
        SDL_UpdateWindowSurface(window);
        SDL_Delay(2000);
        SDL_FreeSurface(image);
        SDL_FreeSurface(surface);
        SDL_DestroyWindow(window);
        SDL_Quit();
        return (EXIT_SUCCESS);
      }
    }
  }
}
