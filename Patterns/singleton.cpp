#include <iostream>

class Server
{

public:
  static Server& getInstance(void)
  {
    static Server ins(8080);
    return ins;
  }

  void setPort(int port) { this->_port = port; }
  void print(void) { std::cout << this->_port << "\n"; }
  virtual ~Server() = default;

private:
  Server& operator=(const Server&) = delete;
  Server() = delete;
  Server(const Server&) = delete;
  explicit Server(int port)
    : _port(port)
  {}
  int _port;
};

int
main(void)
{
  Server& a = Server::getInstance();
  Server& b = Server::getInstance();
  b.setPort(22);
  a.print();
}
