#include <iostream>
#include <map>
#include <memory>
class Proto
{
public:
  virtual ~Proto() = default;
  Proto() = default;
  virtual std::unique_ptr<Proto> clone(void) = 0;
  virtual void print() = 0;
};

class CarProto : public Proto
{
private:
  std::string _name;

public:
  ~CarProto() = default;
  explicit CarProto(const std::string& name) { this->_name = name; }
  std::unique_ptr<Proto> clone(void) override
  {
    return std::make_unique<CarProto>(*this);
  }
  void print() override { std::cout << "I'm a car[" << this->_name << "]\n"; }
};

class ProtoBuilder
{

public:
  enum class Protos
  {
    CarP
  };
  ProtoBuilder()
  {
    this->_objs[ProtoBuilder::Protos::CarP] =
      std::make_unique<CarProto>("Jaguar");
  }
  std::unique_ptr<Proto> getCar()
  {
    return this->_objs[ProtoBuilder::Protos::CarP]->clone();
  }

private:
  std::map<Protos, std::unique_ptr<Proto>> _objs;
};

int
main(void)
{
  auto a = ProtoBuilder().getCar();
  auto b = ProtoBuilder().getCar();
  std::cout << ProtoBuilder::Protos::CarP << "\n";
  a->print();
  b->print();
}
