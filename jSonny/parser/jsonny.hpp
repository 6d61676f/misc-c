#include <json/json.h>
#include <set>
#include <string>
#include <unordered_map>

class jSonny
{
private:
  std::string _configFile;
  Json::Value _jsonData;

public:
  class jVally;

public:
  explicit jSonny(const std::string& file);
  virtual ~jSonny() = default;
  int_fast8_t parseValue(class jSonny::jVally& value);
};

class jSonny::jVally
{
private:
  std::string _typeName;

public:
  const std::string getName() const { return this->_typeName; }
  explicit jVally(const std::string& property);
  virtual ~jVally() = default;
  virtual int_fast8_t parse(Json::Value& val) = 0;
};

// TODO Intermediate class used to deploy a scenario in Vulny
class VulnyScenario
{
private:
  bool _stop{ false };
  bool _sync{ true };

public:
  virtual int_fast8_t deploy() = 0;
  VulnyScenario(bool sync = false);
  virtual ~VulnyScenario() = default;
};

class Stealth
  : public jSonny::jVally
  , public VulnyScenario
{
private:
  std::string _intName{ "eno1" };
  uint32_t _time{ 0 };
  std::string _outFile{ "" };

public:
  explicit Stealth(const std::string& propname);
  virtual ~Stealth() = default;
  int_fast8_t parse(Json::Value& val) override;
  int_fast8_t deploy() override;
  friend std::ostream& operator<<(std::ostream& out, const class Stealth& me);
};

class Knock : public jSonny::jVally
{
private:
  std::string _intName{ "eno1" };
  char _newMac[6]{ 0 };
  uint32_t _newIp{ 0 };
  std::string _outFile{ "" };
  std::unordered_map<uint_fast32_t, std::set<uint16_t>> _tcpPorts;
  std::unordered_map<uint_fast32_t, std::set<uint16_t>> _udpPorts;

private:
  int_fast8_t _parsePorts(
    const Json::Value& val,
    const std::string& opt,
    std::unordered_map<uint_fast32_t, std::set<uint16_t>>& mappy);

public:
  explicit Knock(const std::string& propname);
  virtual ~Knock() = default;
  int_fast8_t parse(Json::Value& val) override;
  friend std::ostream& operator<<(std::ostream& out, const class Knock& me);
};
