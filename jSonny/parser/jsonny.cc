#include "jsonny.hpp"
#include <arpa/inet.h>
#include <fstream>

jSonny::jSonny(const std::string& file)
{
  this->_configFile = file;
  std::ifstream in(this->_configFile, std::ios::in);
  if (in) {
    in >> this->_jsonData;
  } else {
    throw "file is not ok";
  }
}

int_fast8_t
jSonny::parseValue(class jSonny::jVally& value)
{
  const std::string& propertyName = value.getName();
  if (propertyName.length() == 0) {
    return value.parse(this->_jsonData);
  } else {
    return value.parse(this->_jsonData[propertyName.c_str()]);
  }
}

jSonny::jVally::jVally(const std::string& property)
{
  this->_typeName = property;
}

VulnyScenario::VulnyScenario(bool sync)
  : _sync(sync){};

Stealth::Stealth(const std::string& property)
  : jSonny::jVally(property)
  , VulnyScenario()
{
}

int_fast8_t
Stealth::parse(Json::Value& val)
{
  int ret = EXIT_SUCCESS;
  bool done = false;
  while (!done) {
    try {
      this->_time = val.get("time", _time).asUInt();
      this->_intName = val.get("interface", _intName).asString();
      this->_outFile = val.get("outfile", _outFile).asString();
      done = true;
    } catch (const std::exception& ex) {
      fprintf(stderr, "%s\n", ex.what());
      ret = EXIT_FAILURE;
    }
  }
  return ret;
}

int_fast8_t
Stealth::deploy()
{
  auto ret = EXIT_SUCCESS;

  return ret;
}

Knock::Knock(const std::string& propname)
  : jSonny::jVally(propname)
{
}

int_fast8_t
Knock::_parsePorts(const Json::Value& val,
                   const std::string& opt,
                   std::unordered_map<uint_fast32_t, std::set<uint16_t>>& mappy)
{
  auto ret = EXIT_FAILURE;
  auto done = false;
  while (!done) {
    try {
      auto category = val[opt.c_str()];
      if (category.isArray()) {
        for (const auto& entry : category) {
          if (entry.isObject()) {
            auto ip = entry.get("ip_dest", "0.0.0.0").asString();
            uint_fast32_t key{ 0 };
            if (inet_pton(AF_INET, ip.c_str(), &key) != 1 ||
                (key == 0 || key == (uint32_t)-1)) {
              fprintf(stderr, "inet_pton has failed...invalid ip\n");
              continue;
            }
            key = ntohl(key);
            auto ports = entry["ports"];
            if (ports.isArray()) {
              for (const auto port : ports) {
                mappy[key].insert(port.asUInt());
              }
            } else {
              fprintf(stderr, "Ports is not array\n");
              continue;
            }
          } else {
            fprintf(stderr, "Entry is not object\n");
            continue;
          }
        }
      } else {
        fprintf(stderr, "Category is not array\n");
      }
      done = true;
    } catch (const std::exception& ex) {
      fprintf(stderr, "%s\n", ex.what());
      ret = EXIT_FAILURE;
    }
  }
  return ret;
}

int_fast8_t
Knock::parse(Json::Value& val)
{
  int ret = EXIT_SUCCESS;
  bool done = false;
  while (!done) {
    try {
      this->_intName = val.get("interface", _intName).asString();
      this->_outFile = val.get("outfile", _outFile).asString();
      auto newIp = val.get("newip", "0.0.0.0").asString();
      auto newMac = val.get("newmac", "00:00:00:00:00:00").asString();
      this->_parsePorts(val, "tcp", this->_tcpPorts);
      this->_parsePorts(val, "udp", this->_udpPorts);
      done = true;
    } catch (const std::exception& ex) {
      fprintf(stderr, "%s\n", ex.what());
      ret = EXIT_FAILURE;
    }
  }
  return ret;
}

std::ostream&
operator<<(std::ostream& out, const class Stealth& me)
{
  out << "Dumping Stealth Object\n";
  out << "intname: " << me._intName << ", outfile: " << me._outFile
      << ", time: " << me._time;
  out << "\n";
  return out;
}

std::ostream&
operator<<(std::ostream& out, const class Knock& me)
{
  out << "DUMPING Knock Object\n";
  out << "intname: " << me._intName << ", outfile: " << me._outFile
      << ", newMac: " << me._newMac << ", newIp: " << me._newIp << "\n";
  out << "\nTCP:\n";
  for (auto& i : me._tcpPorts) {
    out << "IP Dst: " << i.first << ":\n";
    for (auto& j : i.second) {
      out << j << " ";
    }
    out << "\n";
  }
  out << "\nUDP:\n";
  for (auto& i : me._udpPorts) {
    out << "IP Dst: " << i.first << ":\n";
    for (auto& j : i.second) {
      out << j << " ";
    }
  }
  out << "\n";
  return out;
}
