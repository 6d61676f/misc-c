#include "jsonny.hpp"
#include <iostream>

int
main(void)
{
  try {
    jSonny sonny("../vulny.json");
    Stealth stelz("stealth");
    Knock knocky("knock");
    sonny.parseValue(stelz);
    sonny.parseValue(knocky);
    std::cout << stelz << "\n";
    std::cout << knocky << "\n";

  } catch (std::exception& ex) {
    printf("%s\n", ex.what());
  } catch (const char* msg) {
    printf("%s\n", msg);
  }
}
