#ifndef HELPER_H
#define HELPER_H
#include <stdio.h>
#define printAddr(VAR) printAddrExtra(VAR, "%c", '\n')

#define printAddrExtra(VAR, fmt, ...)                                          \
  do {                                                                         \
    printf("%-15s %p" fmt, #VAR, (void*)VAR, __VA_ARGS__);                     \
  } while (0)

#define printHeader(Name)                                                      \
  do {                                                                         \
    int sName = 0, i = 0;                                                      \
    for (sName = 0; Name[sName] != '\0'; sName++)                              \
      ;                                                                        \
    printf("\n");                                                              \
    for (i = 0; i < 80; i++) {                                                 \
      putchar('-');                                                            \
    }                                                                          \
    printf("\n");                                                              \
    for (i = 0; i < 80 / 2 - sName; i++) {                                     \
      printf(" ");                                                             \
    }                                                                          \
    printf("%s\n", Name);                                                      \
    for (i = 0; i < 80; i++) {                                                 \
      putchar('-');                                                            \
    }                                                                          \
    printf("\n");                                                              \
  } while (0)

int
printProcMMap(void);

void
hasStatic(void);

#endif
