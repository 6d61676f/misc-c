#include "helper_func.h"
#include <stdio.h>
#include <stdlib.h>

#ifndef PRINTMMAP
#define PRINTMMAP
#endif

int gInt;
static int sgInt;

int
main(int argc, char* argv[], char* envp[])
{
  void* val1024 = malloc(1024);
  void* val4096 = malloc(0x1000);
  const char* dataChar = "I'm constant";
  static int sInt;

  printHeader("STACK:");
  for (int i = 0; envp[i] != 0; i++) {
    printAddr(envp[i]);
  }
  for (int i = 0; i < argc; i++) {
    printAddr(argv[i]);
  }
  printAddr(envp);
  printAddr(argv);
  printAddr(&val1024);
  printAddr(&val4096);
  printAddr(&dataChar);

  printHeader("HEAP:");
  printAddr(val4096);
  printAddr(val1024);

  printHeader("DATA:");
  printAddr(&gInt);
  printAddr(&sInt);
  hasStatic();
  printAddr(&sgInt);
  printAddr(dataChar);
  printAddr(main);
  printAddr(printf);

#ifndef PRINTMMAP
  printf("\n\ncat /proc/%d/maps\n\n", getpid());
  getchar();
#else
  printProcMMap();
#endif

  free(val4096);
  free(val1024);
  return (EXIT_SUCCESS);
}
