#include "helper_func.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
int
printProcMMap(void)
{
  size_t buf_size = 0x100;
  char* buffer = malloc(buf_size);
  snprintf(buffer, buf_size - 1, "/proc/%d/maps", getpid());
  FILE* f = fopen(buffer, "r");
  if (f) {
    puts("\n");
    while (getline(&buffer, &buf_size, f) != -1) {
      printf("%s", buffer);
    }
    fclose(f);
  } else {
    return (EXIT_FAILURE);
  }
  free(buffer);
  return (EXIT_SUCCESS);
}
void
hasStatic(void)
{
  static int sIntInF;
  printAddr(&sIntInF);
}
