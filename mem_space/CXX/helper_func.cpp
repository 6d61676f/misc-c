#include "helper_func.hpp"
#include <fstream>
#include <iostream>
#include <sstream>

std::shared_ptr<std::map<std::ptrdiff_t, std::string>>
createMap()
{
  return std::make_shared<std::map<std::ptrdiff_t, std::string>>();
}

int
printMMap(int pid)
{
  std::ifstream in;
  std::string line;
  std::ostringstream path;
  path << "/proc/" << pid << "/maps";
  in.open(path.str().c_str(), std::ios_base::in);
  if (in.is_open()) {
    while (std::getline(in, line)) {
      std::cout << line << "\n";
    }
    in.close();
    return 0;
  } else {
    std::cout << path.str() << " cannot be opened.\nTry pmap " << pid;
    return 1;
  }
}

int
printItems(const std::map<std::ptrdiff_t, std::string>& hmap, size_t tWidth)
{
  int counter = 0;
  const std::string stackMsg("Stack Growing Downwards");
  const std::string dataMsg("Data + Heap Growing Upwards");
  std::cout << std::string(tWidth, '-') << "\n";
  std::cout << std::string((tWidth - stackMsg.size()) / 2, ' ') << stackMsg
            << "\n";
  std::cout << std::string(tWidth, '-') << "\n";
  std::for_each(hmap.rbegin(), hmap.rend(), [&](decltype(*hmap.begin())& i) {
    counter++;
    std::printf(
      "%-*s %*p\n", tWidth / 2, i.second.c_str(), tWidth / 3, i.first);
  });
  std::cout << std::string(tWidth, '-') << "\n";
  std::cout << std::string((tWidth - dataMsg.size()) / 2, ' ') << dataMsg
            << "\n";
  std::cout << std::string(tWidth, '-') << "\n";
  return counter;
}
