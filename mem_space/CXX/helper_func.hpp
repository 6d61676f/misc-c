#ifndef HELPER_HPP
#define HELPER_HPP
#include <algorithm>
#include <iostream>
#include <map>
#include <memory>
#include <unistd.h>

#define addItem(hash, var)                                                     \
  do {                                                                         \
    hash.get()->insert(                                                        \
      std::make_pair((std::ptrdiff_t)var, std::string(#var)));                 \
  } while (0)

#define addNamedItem(hash, var, varName)                                       \
  do {                                                                         \
    hash.get()->insert(                                                        \
      std::make_pair((std::ptrdiff_t)var, std::string(varName)));              \
  } while (0)

int
printMMap(int pid = getpid());

int
printItems(const std::map<std::ptrdiff_t, std::string>& hmap,
           size_t tWidth = 80);

std::shared_ptr<std::map<std::ptrdiff_t, std::string>>
createMap();

#endif
