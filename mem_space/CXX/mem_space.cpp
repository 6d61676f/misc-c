#include "helper_func.hpp"
#include <fcntl.h>
#include <iostream>
#include <map>
#include <memory>
#include <sstream>
#include <sys/mman.h>
#include <sys/stat.h>

int gloInt = 5;
static int gsInt;

int
main(int argc, char* argv[], char* envp[])
{
  auto items = createMap();
  addItem(items, argv);

  const char* textChar = "Text";
  addItem(items, &textChar);
  addItem(items, textChar);

  std::ostringstream formatter;
  addItem(items, &formatter);

  auto heapMap = std::make_shared<decltype(items)>(items);
  addItem(items, &heapMap);
  addItem(items, heapMap.get());

  std::shared_ptr<int> sharedInt(new int[0x100], std::default_delete<int[]>());
  addItem(items, sharedInt.get());
  addItem(items, &sharedInt);

  std::shared_ptr<char> sharedChar(new char[200]{ '/', 's', 'h', 'm' },
                                   std::default_delete<char[]>());

  int fo = fileno(tmpfile());
  if (fo != -1) {
    void* memMapShared = mmap(0x0, 0x1000, PROT_READ, MAP_SHARED, fo, 0);
    void* memMapPrivate = mmap(0x0, 0x500, PROT_READ, MAP_PRIVATE, fo, 0);
    if (memMapShared != MAP_FAILED) {
      addItem(items, memMapShared);
    }
    if (memMapPrivate != MAP_FAILED) {
      addItem(items, memMapPrivate);
    }
  }
  int shmfd = shm_open(sharedChar.get(), O_RDWR | O_CREAT | O_TRUNC, 0666);
  ftruncate(shmfd, 0x2000);
  if (shmfd != -1) {
    void* memMapShm =
      mmap(0x0, 0x2000, PROT_READ | PROT_WRITE, MAP_SHARED, shmfd, 0);
    if (memMapShm != MAP_FAILED) {
      addItem(items, memMapShm);
    }
    close(shmfd);
  }

  static const int scInt = 0;
  addItem(items, &scInt);

  static int sInt;
  addItem(items, &sInt);

  addItem(items, &gloInt);
  addItem(items, &gsInt);

  addItem(items, main);
  addItem(items, printMMap);

  addItem(items, &argc);
  addItem(items, argv);
  for (auto i = 0; i < argc; i++, formatter.str("")) {
    formatter << "argv[" << i << "]:";
    formatter << std::string(argv[i]).substr(0, 39 - formatter.str().size());
    addNamedItem(items, argv[i], formatter.str());
  }

  addItem(items, envp);
  for (auto i = 0; envp[i]; i++, formatter.str("")) {
    formatter << "envp[" << i << "]:";
    formatter << std::string(envp[i]).substr(0, 39 - formatter.str().size());
    addNamedItem(items, envp[i], formatter.str());
  }

  addItem(items, &std::cout);
  printItems(*items.get());
  std::cout << "\n\n";
  printMMap();

  return 0;
}
