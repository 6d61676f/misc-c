#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

int
main(void)
{
  struct timespec start = { 0 }, end = { 0 }, res = { 0 };
  clock_gettime(CLOCK_MONOTONIC, &start);
  for (int i = 0; i < INT_MAX; i++) {
  }
  clock_gettime(CLOCK_MONOTONIC, &end);
  res.tv_sec = end.tv_sec - start.tv_sec;
  res.tv_nsec = end.tv_nsec - start.tv_nsec;
  printf("%f\n", res.tv_sec + (float)res.tv_nsec / 1000000000);
  return 0;
}
