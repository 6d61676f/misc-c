#include "ringbuffer.hpp"
#include <iostream>

int
main(void)
{
  try {

    RingBuffer<int> buffer{ 1, 2, 3, 4, 5 };
    buffer.add(1);
    buffer.add({ 10, 11, 12, 13, 14 });
    for (RingBuffer<int>::iterator i = buffer.begin(); i != buffer.end(); ++i) {
      std::cout << *i << " ";
    }
    std::cout << std::endl;

    buffer.add(-1);
    for (auto i : buffer) {
      std::cout << i << " ";
    }
    std::cout << std::endl;

    RingBuffer<int> nou{ buffer }; // sau nou=buffer , sau nou(buffer)
    std::cout << nou;

  } catch (std::exception& ex) {
    std::cerr << __PRETTY_FUNCTION__ << ": " << ex.what() << std::endl;
    return (EXIT_FAILURE);
  }
  return (EXIT_SUCCESS);
}
