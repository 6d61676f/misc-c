#ifndef RINGBUFFER_HPP
#define RINGBUFFER_HPP

#include <initializer_list>
#include <iostream>
#include <stdexcept>

template<typename T>
class RingBuffer
{
public:
  class iterator;

  RingBuffer() = delete;

  explicit RingBuffer(std::size_t size)
    : _size(size)
  {
    try {
      if (!size)
        throw std::invalid_argument("Zero size");
      else {
        this->_array = new T[this->_size];
      }
    } catch (std::exception& ex) {
      std::cerr << __PRETTY_FUNCTION__ << ": " << ex.what() << std::endl;
      throw;
    }
  }

  RingBuffer(const RingBuffer& other)
    : _size(other._size)
    , _indexCurent(other._indexCurent)
  {
    try {
      this->_array = new T[this->_size];
      for (unsigned i = 0; i < this->_size; i++) {
        this->_array[i] = other._array[i];
      }
    } catch (std::exception& ex) {
      std::cerr << ex.what() << std::endl;
      throw;
    }
  }

  RingBuffer<T>& operator=(const RingBuffer<T>& other)
  {
    if (this == &other) {
      return *this;
    }

    if (_array) {
      delete[] _array;
    }
    _size = other._size;
    _indexCurent = other._indexCurent;
    try {
      _array = new T[_size];
      for (unsigned i = 0; i < _size; i++) {
        this->_array[i] = other._array[i];
      }
    } catch (std::exception& ex) {
      std::cerr << ex.what() << std::endl;
      throw;
    }
    return *this;
  }

  explicit RingBuffer(std::initializer_list<T> lista)
    : _size(lista.size())
  {
    std::uint32_t index{ 0 };
    try {
      this->_array = new T[this->_size]{};
      for (T i : lista) {
        this->_array[index++] = i;
      }
      this->_indexCurent = (this->_size - 1);
    } catch (std::exception& ex) {
      std::cerr << ex.what() << std::endl;
      throw;
    }
  }

  bool add(T element)
  {
    bool status = false;
    if (this->_indexCurent == this->_size - 1) {
      status = true;
    }

    if (this->_indexCurent < this->_size - 1) {
      this->_indexCurent++;
    } else {
      this->_indexCurent = 0;
    }

    this->_array[this->_indexCurent] = element;

    return (status);
  }

  bool add(std::initializer_list<T> elemente)
  {
    bool status =
      (this->_indexCurent + elemente.size() >= this->_size) ? true : false;

    for (T element : elemente) {

      if (this->_indexCurent < this->_size - 1) {
        this->_indexCurent++;
      } else {
        this->_indexCurent = 0;
      }

      this->_array[this->_indexCurent] = element;
    }

    return (status);
  }

  std::size_t getSize() const { return (this->_size); }

  virtual ~RingBuffer()
  {
    try {
      if (this->_array != nullptr) {
        delete[] this->_array;
      }
    } catch (std::exception& ex) {
      std::cerr << __PRETTY_FUNCTION__ << ": " << ex.what() << std::endl;
    }
  }

  friend std::ostream& operator<<(std::ostream& out, const RingBuffer<T>& obj)
  {
    for (std::uint32_t i = 0; i < obj._size; i++) {
      out << obj._array[i] << " ";
    }
    out << std::endl;
    return (out);
  }

  iterator begin() { return iterator(0, *this); }
  iterator end() { return iterator(this->_size, *this); }

private:
  T* _array{ nullptr };
  const std::size_t _size;
  std::uint32_t _indexCurent{ 0 };
};

template<typename T>
class RingBuffer<T>::iterator
{
private:
  std::uint32_t _index;
  RingBuffer<T>& _buffer;

public:
  iterator(std::uint32_t index, RingBuffer<T>& buffer)
    : _index(index)
    , _buffer(buffer)
  {}

  iterator(const iterator& other)
    : _index(other._index)
    , _buffer(other._buffer)
  {}

  iterator() = default;

  virtual ~iterator() = default;

  T& operator*() { return _buffer._array[_index]; }

  bool operator!=(const RingBuffer<T>::iterator& other) const
  {
    return (this->_index != other._index);
  }

  iterator& operator=(const iterator& other)
  {
    this->_index = other._index;
    this->_buffer = other._buffer;
    return (*this);
  }

  iterator& operator++(int)
  {
    _index++;
    return (*this);
  }

  iterator& operator++()
  {
    _index++;
    return (*this);
  }
};

#endif // RINGBUFFER_HPP
