#include <limits.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

/* Siginfo_t prettyprint */
void printSiginfo(const siginfo_t);
/* Handler function */
static void handler(int);
/* Cycle consuming function */
void consume(void);

int
main(void)
{
    /* Struct sigaction holds the handler, operation flags and masked (blocked)
     * signals*/
    /* Blocked signals are added with sigaddset and it's mandatory to call
     * sigprocmask afterwards */
    struct sigaction sa = { 0 };
    sa.sa_flags = 0;
    sa.sa_handler = handler;
    sigemptyset(&sa.sa_mask);
    sigaddset(&sa.sa_mask, SIGINT);
    sigaddset(&sa.sa_mask, SIGSEGV);
    sigprocmask(SIG_BLOCK, &sa.sa_mask, 0);

    /* We are handling SIGINT , SIGSEGV , SIGALRM and SIGHUP. The first two
     * synchronously */
    if (sigaction(SIGINT, &sa, 0)) {
        fprintf(stderr, "Problem at siagction SIGINT\n");
        return (EXIT_FAILURE);
    }
    if (sigaction(SIGSEGV, &sa, 0)) {
        fprintf(stderr, "Problem at siagction SIGSEGV\n");
        return (EXIT_FAILURE);
    }
    if (sigaction(SIGALRM, &sa, 0)) {
        fprintf(stderr, "Problem at siagction SIGSEGV\n");
        return (EXIT_FAILURE);
    }
    if (sigaction(SIGHUP, &sa, 0)) {
        fprintf(stderr, "Problem at siagction SIGHUP\n");
        return (EXIT_FAILURE);
    }
    printf("Waiting for SIGSEGV (kill -SEGV) , SIGINT (^c), SIGALRM (kill "
           "-ALRM) and SIGHUP (kill -HUP). In order to exit: ^\\ or SIGHUP\n");
    printf("My PID is %d\n", getpid());

    /* Timeout for the signal wait function */
    struct timespec wait;
    wait.tv_sec = 5;
    wait.tv_nsec = 0;

    siginfo_t sinfo = { 0 };

    /* Infinte loop in which we handle the signals and then wait */
    while (1) {

        if (sigtimedwait(&sa.sa_mask, &sinfo, &wait) > 0) {
            printSiginfo(sinfo);
        }

        consume();
    }
}

void
handler(int signum)
{
    size_t bufSize = 30;
    char buffer[bufSize];
    memset(buffer, 0x0, bufSize);
    snprintf(buffer, bufSize, "Async got: %d\n", signum);
    write(STDOUT_FILENO, buffer, bufSize);
    if (signum == SIGHUP) {
        exit(0);
    }
}

void
printSiginfo(const siginfo_t sinfo)
{
    printf("\nSIGINFO Sync:\n");
    printf("No: %d\n", sinfo.si_signo);
    printf("Errno: %d\n", sinfo.si_errno);
    printf("Code: %d\n", sinfo.si_code);
    printf("Value: %d\n", sinfo.si_value.sival_int);
    printf("Pid: %d\n", sinfo.si_pid);
    printf("Uid: %d\n", sinfo.si_uid);
    printf("Faulting instruction: %p\n", sinfo.si_addr);
    printf("Exit status: %d\n", sinfo.si_status);
    printf("Band Event: %lu\n", sinfo.si_band);
    printf("TimerId: %d\n", sinfo.si_timerid);
    printf("Overrun count: %d\n\n", sinfo.si_overrun);
}

void
consume(void)
{
    printf("Start of consume\n");
    unsigned i;
    for (i = 0; i < UINT_MAX / 2; i++) {
    }
    printf("End of consume\n");
}
