#include <limits.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

/* Handler function */
static void handler(int);
/* Cycle consuming function */
void consume(void);

int
main(void)
{
  /* Struct sigaction holds the handler, operation flags and masked (blocked)
   * signals*/
  /* Blocked signals are added with sigaddset and it's mandatory to call
   * sigprocmask afterwards */
  struct sigaction sa = { 0 };
  sa.sa_flags = 0;
  sa.sa_handler = handler;
  sigemptyset(&sa.sa_mask);
  sigprocmask(SIG_BLOCK, &sa.sa_mask, 0);

  /* We are handling SIGINT , SIGSEGV , SIGALRM and SIGHUP. */
  if (sigaction(SIGINT, &sa, 0)) {
    fprintf(stderr, "Problem at siagction SIGINT\n");
    return (EXIT_FAILURE);
  }
  if (sigaction(SIGSEGV, &sa, 0)) {
    fprintf(stderr, "Problem at siagction SIGSEGV\n");
    return (EXIT_FAILURE);
  }
  if (sigaction(SIGALRM, &sa, 0)) {
    fprintf(stderr, "Problem at siagction SIGSEGV\n");
    return (EXIT_FAILURE);
  }
  if (sigaction(SIGHUP, &sa, 0)) {
    fprintf(stderr, "Problem at siagction SIGHUP\n");
    return (EXIT_FAILURE);
  }
  printf("Waiting for SIGSEGV (kill -SEGV) , SIGINT (^c), SIGALRM (kill "
         "-ALRM) and SIGHUP (kill -HUP). In order to exit: ^\\ or SIGHUP\n");
  printf("My PID is %d\n", getpid());

  /* Infinte loop to mimic normal execution */
  while (1) {
    consume();
  }
}

void
handler(int signum)
{
  size_t bufSize = 30;
  char buffer[bufSize];
  memset(buffer, 0x0, bufSize);
  snprintf(buffer, bufSize, "Async got: %d\n", signum);
  write(STDOUT_FILENO, buffer, bufSize);
  if (signum == SIGHUP) {
    exit(0);
  }
}

void
consume(void)
{
  printf("Start of consume\n");
  unsigned i;
  for (i = 0; i < UINT_MAX / 2; i++) {
  }
  printf("End of consume\n");
}
