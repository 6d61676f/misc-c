#include "benum.hpp"

#include <memory>
#include <stdexcept>
#include <unordered_map>
#include <unordered_set>

namespace TRAFFIC {

class EnumImpl final : public Enum
{
private:
  const Enum::Val m_val;
  const Enum::Name m_name;

public:
  EnumImpl(Enum::Val val, const Enum::Name& name)
    : m_val(val)
    , m_name(name)
  {}
  virtual Enum::Val get_val() const override { return m_val; }
  virtual const Enum::Name& get_name() const override { return m_name; }
  virtual ~EnumImpl() {}
};

class _BENUM
{
private:
  static std::unordered_map<Enum::Val, const std::shared_ptr<const Enum>>
    m_val_to_type;
  static std::unordered_map<Enum::Name, const std::shared_ptr<const Enum>>
    m_name_to_type;

public:
  static Enum::Type add_enum(const Enum::Name& n, Enum::Val v)
  {
    auto en = std::make_shared<EnumImpl>(v, n);
    if (!m_val_to_type.insert({ v, en }).second) {
      throw std::domain_error("value " + std::to_string(v) + " is not unique.");
    }

    if (!m_name_to_type.insert({ n, en }).second) {
      throw std::domain_error("name " + n + " is not unique!");
    }
    return *m_val_to_type.at(v);
  }
  static Enum::Type get(Enum::Val v)
  {
    auto t = m_val_to_type.find(v);
    if (t == m_val_to_type.end()) {
      throw std::domain_error("Value " + std::to_string(v) +
                              " is not a registered enum entry!");
    }
    return *t->second;
  }
  static Enum::Type get(const Enum::Name& n)
  {
    auto t = m_name_to_type.find(n);
    if (t == m_name_to_type.end()) {
      throw std::domain_error("Name " + n + " is not a registered enum entry!");
    }
    return *t->second;
  }
} BENUMS;

std::unordered_map<Enum::Val, const std::shared_ptr<const Enum>>
  _BENUM::m_val_to_type;
std::unordered_map<Enum::Name, const std::shared_ptr<const Enum>>
  _BENUM::m_name_to_type;

Enum::Type
Enum::from(Enum::Val v)
{
  return BENUMS.get(v);
}

Enum::Type
Enum::from(const Enum::Name& n)
{
  return BENUMS.get(n);
}

#define DEF_BENUM(name, val) Enum::Type name = BENUMS.add_enum(#name, val)
DEF_BENUM(INGRESS, 0);
DEF_BENUM(EGRESS, 1);
#undef DEF_BENUM

}
