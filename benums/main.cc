#include "benum.hpp"
#include <cstdlib>
#include <exception>
#include <iostream>

int
main(void)
{
  try {
    TRAFFIC::Enum::Type t = TRAFFIC::INGRESS;
    std::cout << t << "\n";
    std::cout << TRAFFIC::Enum::from(0) << "\n";
    std::cout << TRAFFIC::Enum::from(1) << "\n";
    auto& invalid = TRAFFIC::Enum::from("REGRESS");
    std::cout << invalid << "\n";
  } catch (const std::exception& ex) {
    std::cerr << ex.what() << "\n";
  }
  return EXIT_SUCCESS;
}
