#pragma once

#include <cstdint>
#include <ostream>
#include <string>

namespace TRAFFIC {

class Enum
{
  // Types
public:
  using Val = std::uint64_t;
  using Name = std::string;
  using Type = const Enum&;

  // Getters
public:
  virtual Val get_val() const = 0;
  virtual const Name& get_name() const = 0;
  virtual ~Enum() = default;

  // Convertors
public:
  static Enum::Type from(Enum::Val);
  static Enum::Type from(const Enum::Name&);

public:
  friend std::ostream& operator<<(std::ostream& out, const class Enum& other)
  {
    out << other.get_name() << std::string(":")
        << std::to_string(other.get_val());
    return out;
  }
};

#define DECL_BENUM(name, val) extern Enum::Type name
DECL_BENUM(INGRESS, 0);
DECL_BENUM(EGRESS, 1);
#undef DECL_BENUM

}
