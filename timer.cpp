#include <chrono>
#include <climits>
#include <iostream>

typedef std::chrono::high_resolution_clock hrc;

int
main(void)
{
  auto start = hrc::now();
  for (int i = 0; i < INT_MAX; i++) {
  }
  auto end = hrc::now();
  auto res = end - start;
  auto milis =
    std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
  std::cout << milis.count() << "\n";
  std::cout << res.count() << "\n";
  return 0;
}
