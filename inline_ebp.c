#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void* mainAddr;

struct moloz
{
  int b;
  char a;
};

int
funky(int a, int b)
{
  extern int main();
  void** cebp;
  void** cretur;
  char d = a + b;
  printf("%s\n", isalnum(d) ? "Yes" : "No");
  // Getting the value of EBP register in a variable
  asm volatile("mov %%ebp, %0" : "=r"(cebp));
  // Getting the return address of the function in a variable
  asm volatile("mov 4(%%ebp), %0" : "=r"(cretur));
  printf("%p %p\n", cebp, cretur);
  printf("cebp + 1 (i.e sizeof(void*)) %p\n", *(cebp + 1));
  // Setting the return address as zero
  // memset(cebp + 1, 0x00, sizeof *cebp);
  // Setting the return address as main
  //*(cebp + 1) = mainAddr;
  //// Or
  // memcpy((cebp + 1), &mainAddr, sizeof *cebp);
  ////Or
  *(cebp + 1) = (void*)main;
  printf("cebp + 1 (i.e sizeof(void*)) %p\n", *(cebp + 1));
  printf("Shall return\n");
  return (a + b);
}

int
main()
{
  mainAddr = (void*)main;
  struct moloz c;
  printf("%zu %zu %zu\n",
         sizeof c,
         (size_t) & ((struct moloz*)0)->a,
         (size_t) & ((struct moloz*)0)->b);
  funky(1, 2);

  return 0;
}
