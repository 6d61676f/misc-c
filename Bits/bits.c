#include "bits.h"
#include <inttypes.h>
#include <stdio.h>
int
printBits(uintmax_t no)
{
    uintmax_t placeholder = no;
    uint8_t maxSetBit = 0;

    printf("\n%ju -- 0X%jX :\n", no, no);
    for (maxSetBit = 0; placeholder != 0; placeholder >>= 1, maxSetBit++) {
    }
    placeholder = no;
    printf("\n");
    for (uint8_t i = 1; i <= maxSetBit; i++) {
        printf("%02d|", maxSetBit - i);
    }
    printf("\n");
    for (uint8_t i = 1; i <= maxSetBit; i++) {
        printf("%2d|", (placeholder & (1 << (maxSetBit - i))) ? 1 : 0);
    }
    printf("\n");
    return 1;
}
