#include "bits.h"
#include <stdio.h>

int
main(int argc, char* argv[])
{
    if (argc < 2) {
        fprintf(stderr, "%s #1 #2 #n\n", argv[0]);
        return 1;
    } else {
        uintmax_t no;
        for (int i = 1; i < argc; i++) {
            if (argv[i][1] == 'X' || argv[i][1] == 'x') {
                no = strtoumax(argv[i], NULL, 16);
            } else {
                no = strtoumax(argv[i], NULL, 10);
            }
            printBits(no);
        }
    }
    return 0;
}
