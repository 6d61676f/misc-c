#include <algorithm>
#include <fstream>
#include <iostream>
#include <vector>

void
obfuscate(const std::string& pass, std::vector<uint8_t>& output)
{
  // Odds
  for (size_t index = 1; index < pass.size(); index += 2) {
    uint8_t temp = pass.at(index);
    temp ^= (0x65u - 1);
    if (temp == 0x1c) {
      temp ^= (0x56u - 1);
    }
    output.push_back(temp);
  }

  output.push_back(0x1c); // evens start after me
  // Evens
  for (size_t index = 0, odd = 0; index < pass.size(); index += 2, odd++) {
    uint8_t temp = pass.at(index);
    uint16_t next = output.at(odd);
    if (next == 0x1c) {
      std::cout << std::string{ "\nOB - Should enter just once...!\n" };
      temp ^= 0x47u;
    } else {
      for (uint16_t j = 1; j <= next; j <<= 3u) {
        temp ^= j;
      }
    }
    output.push_back(temp);
  }
}

void
de_obfs(std::string& pass, const std::vector<uint8_t>& obfs)
{
  std::vector<uint8_t> bytes(obfs.size() -
                             1); // without the separator between odds and evens
  size_t even_start = 0;
  while (obfs[even_start++] != 0x1c) {
  }

  // Evens
  for (size_t odd = 0, even = 0, even_index = even_start;
       even_index < obfs.size();
       even_index++, odd++, even += 2) {
    uint8_t temp = obfs.at(even_index);
    if (obfs[odd] == 0x1c) {
      std::cout << std::string{ "\nDEOB - Should enter just once...!\n" };
      temp ^= 0x47u;
    } else {
      const uint8_t next = obfs[odd];
      for (uint16_t j = 1; j <= next; j <<= 3u) {
        temp ^= j;
      }
    }
    bytes[even] = temp;
  }

  // Odds
  for (size_t offset = 0, index = 1;
       offset < obfs.size() && obfs[offset] != 0x1c;
       offset++) {
    uint8_t temp = obfs[offset];
    if ((temp ^ (0x56u - 1)) == 0x1c) {
      temp ^= (0x56u - 1);
    }
    temp ^= (0x65u - 1);
    bytes[index] = temp;
    index += 2;
  }
  for_each(bytes.begin(), bytes.end(), [&](auto c) { pass.push_back(c); });
}

int
main(int argc, char* argv[])
{
  if (argc != 2) {
    fprintf(stderr, "%s <pass>\n", argv[0]);
    exit(1);
  }
  std::string pass(argv[1]);
  std::string pass2;
  std::vector<uint8_t> fucked_up;

  obfuscate(pass, fucked_up);
  std::for_each(
    fucked_up.begin(), fucked_up.end(), [](auto c) { printf("%#x, ", c); });
  de_obfs(pass2, fucked_up);
  printf("\nORIG:%s - %zu\n", pass.c_str(), pass.size());
  printf("DEOB:%s - %zu\n", pass2.c_str(), pass2.size());

  std::ofstream out("./output.bin", std::ios::binary | std::ios::out);
  for_each(fucked_up.begin(), fucked_up.end(), [&](uint8_t c) {
    out.write((const char*)&c, sizeof c);
  });

  return 0;
}
