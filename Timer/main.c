#include <limits.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

static volatile sig_atomic_t STOP = 0;

/* Signal Handler */
static void
handler(int signo, siginfo_t* info, void* ucontext);

/* Cycle consuming function */
void
consume(void);

int
main(void)
{
  /* Sigaction catching SIGALRM and SIGINT */
  struct sigaction sa;
  memset(&sa, 0x00, sizeof sa);

  sa.sa_flags = SA_SIGINFO;
  sa.sa_sigaction = handler;
  sigemptyset(&sa.sa_mask);
  sigprocmask(SIG_BLOCK, &sa.sa_mask, 0);

  if (sigaction(SIGALRM, &sa, 0) != 0) {
    fprintf(stderr, "Error at sigaction SIGALRM\n");
    return (EXIT_FAILURE);
  }
  if (sigaction(SIGINT, &sa, 0) != 0) {
    fprintf(stderr, "Error at sigaction SIGINT\n");
    return (EXIT_FAILURE);
  }

  /* Creating Timer */
  timer_t timer;
  struct sigevent sigev;
  memset(&sigev, 0x00, sizeof sigev);
  sigev.sigev_signo = SIGALRM;
  sigev.sigev_value.sival_int = 666;
  sigev.sigev_notify = SIGEV_SIGNAL;

  if (timer_create(CLOCK_MONOTONIC, &sigev, &timer) < 0) {
    fprintf(stderr, "Error at timer_create()\n");
    return (EXIT_FAILURE);
  }

  /* Itimerspec in which we set the first going off value and the next in the
   * case of populating it_interval*/
  struct itimerspec itimer;
  itimer.it_interval.tv_sec = 5;
  itimer.it_interval.tv_nsec = 0;
  itimer.it_value.tv_sec = 3;
  itimer.it_value.tv_nsec = 0;

  if (timer_settime(timer, 0, &itimer, 0) < 0) {
    fprintf(stderr, "Error at timer_settime()\n");
    return (EXIT_FAILURE);
  }

  while (!STOP) {
    consume();
  }
  timer_delete(timer);
  return EXIT_SUCCESS;
}

static void
handler(int signo, siginfo_t* info, void* ucontext)
{
  printf("We caught:\n\tinfo.si_code(%d), info.si_errno(%d), "
         "info.si_signo(%d), info.sival_int(%d)\n",
         info->si_code,
         info->si_errno,
         info->si_signo,
         info->si_value.sival_int);
  if (signo == SIGINT) {
    STOP = 1;
  }
}

void
consume(void)
{
  printf("Start of consume\n");
  printf("Stop with\nkill -INT %u\n", (unsigned)getpid());
  for (unsigned i = 0; i < UINT_MAX / 2; i++) {
  }
  printf("End of consume\n");
}
