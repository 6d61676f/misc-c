#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

/*https://en.wikipedia.org/wiki/Linear-feedback_shift_register*/
void lfsr(uint64_t, uint8_t);

int
main(void)
{
  lfsr(0xAB, 8);
  return (EXIT_SUCCESS);
}

void
lfsr(uint64_t seed, uint8_t len)
{
  if (len < 8) {
    fprintf(stderr, "%s\n", "At least 8 bit length");
    return;
  } else if (len > 64) {
    fprintf(stderr, "%s\n", "At most 64 bit length");
    return;
  } else {
    len--;
  }
  uint64_t count = 0;
  uint64_t no = seed;
  uint64_t mask = (1 << len);
  do {
    const uint8_t first = no & 1;
    const uint8_t second = (no >> 2) & 1;
    const uint8_t third = (no >> 3) & 1;
    const uint8_t forth = (no >> 5) & 1;
    const uint8_t last = first ^ second ^ third ^ forth;
    no >>= 1;
    no = (no & ~mask) | ((last << len) & mask);
    printf("%lu : %lu\n", ++count, no);
  } while (no != seed);
}
