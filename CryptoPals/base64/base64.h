#ifndef BASE64_H
#define BASE64_h
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

typedef union quad2triple quad2triple_t;
union quad2triple
{
  uint32_t one32;
  uint8_t four8[4];
};

uint32_t switchEndian(uint32_t);
void encode64(void*, size_t, void**, size_t*);

#endif
