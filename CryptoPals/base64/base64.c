#include "base64.h"
#include <math.h>

const uint8_t lookUp[] = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
                           'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
                           'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd',
                           'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                           'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
                           'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7',
                           '8', '9', '+', '/' };

void
encode64(void* data, size_t len, void** encodedData, size_t* encodedLen)
{
  uint8_t writeFlag = 0;
  uint8_t* encodedDataCast = NULL;
  if (encodedData && encodedLen) {
    writeFlag = 1;
    *encodedLen = (len + 2) / 3 * 4;
    *encodedData = malloc(*encodedLen);
    encodedDataCast = *encodedData;
  } else {
    return;
  }
  size_t counterEncoded = 0;
  size_t counter = 0;
  quad2triple_t* it;
  for (it = data; counter < len - 2;
       counter += 3, it = (quad2triple_t*)&it->four8[3]) {
    uint32_t tripletNumber = it->one32;
#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
    tripletNumber = switchEndian(tripletNumber);
#endif
    for (uint8_t j = 0; j < 4; j++) {
      uint8_t number = (tripletNumber >> 26) & (0xFF);
      if (writeFlag) {
        encodedDataCast[counterEncoded++] = lookUp[number];
      }
      printf("%c", lookUp[number]);
      tripletNumber <<= 6;
    }
  }
  if (counter == len - 1) {
    uint8_t first = 0, second = 0;
#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
    first = (it->four8[0] >> 2);
    second = (it->four8[0] << 4) & 0x3F;
#elif __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
    first = (it->four8[3] >> 2);
    second = (it->four8[3] << 4) & 0x3F;
#endif
    printf("%c%c==", lookUp[first], lookUp[second]);
    if (writeFlag) {
      encodedDataCast[counterEncoded++] = lookUp[first];
      encodedDataCast[counterEncoded++] = lookUp[second];
      encodedDataCast[counterEncoded++] = '=';
      encodedDataCast[counterEncoded++] = '=';
    }
  } else if (counter == len - 2) {
    uint16_t numero = 0;
#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
    numero = (it->four8[0] << 8) | (it->four8[1]);
#elif __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
    numero = (it->four8[3] << 8) | (it->four8[2]);
#endif
    uint8_t first = numero >> 10;
    uint8_t second = (numero >> 4) & 0x3F;
    uint8_t third = (numero << 2);
    printf("%c%c%c=", lookUp[first], lookUp[second], lookUp[third]);
    if (writeFlag) {
      encodedDataCast[counterEncoded++] = lookUp[first];
      encodedDataCast[counterEncoded++] = lookUp[second];
      encodedDataCast[counterEncoded++] = lookUp[third];
      encodedDataCast[counterEncoded++] = '=';
    }
  }
}

uint32_t
switchEndian(uint32_t number)
{
  return (((number >> 24) & 0xFF) | ((number << 8) & 0xFF0000) |
          ((number >> 8) & 0xFF00) | ((number << 24) & 0xFF000000));
}
