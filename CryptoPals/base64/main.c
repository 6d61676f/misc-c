#include <stdio.h>
#include <stdlib.h>
#include <base64.h>
int
main(void)
{
  uint8_t buffer[48];
  uint8_t* encodedBuffer;
  size_t len;
  FILE* f = fopen("string.dat", "rb");
  if (f) {
    fread(buffer, 1, 48, f);
    fclose(f);
  }
  encode64(buffer, 48, (void**)&encodedBuffer, &len);
  printf("\n");
  for (size_t i = 0; i < len; i++) {
    printf("%c", encodedBuffer[i]);
  }
  if (encodedBuffer) {
    free(encodedBuffer);
  }
  printf("\n");
}
