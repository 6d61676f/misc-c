#include <sort.hpp>
#include <iostream>

template <typename T>
void
sort<T>::mergeSort(T* array, std::uint32_t startIndex, std::uint32_t endIndex)
{
  if (endIndex - startIndex < 2) {
    sort<T>::flip(array, startIndex, endIndex);
  } else {
    std::uint32_t mijloc = (startIndex + endIndex) / 2;
    sort<T>::mergeSort(array, startIndex, mijloc);
    sort<T>::mergeSort(array, mijloc + 1, endIndex);
    sort<T>::interclasare(array, startIndex, mijloc, endIndex);
  }
}

template <typename T>
void
sort<T>::merge(T* array, std::uint32_t startIndex, std::uint32_t endIndex)
{
  sort<T>::arrayIntermediar = new T[endIndex + 1];
  sort<T>::mergeSort(array, startIndex, endIndex);
  delete[] sort<T>::arrayIntermediar;
}
template <typename T>
void
sort<T>::interclasare(T* array, std::uint32_t start, std::uint32_t mid,
                      std::uint32_t end)
{
  auto i = start;
  auto j = mid + 1;
  auto k = start;

  while ((i <= mid) && (j <= end)) {
    if (array[i] <= array[j]) {
      arrayIntermediar[k++] = array[i++];
    } else {
      arrayIntermediar[k++] = array[j++];
    }
  }
  while (i <= mid) {
    arrayIntermediar[k++] = array[i++];
  }
  while (j <= end) {
    arrayIntermediar[k++] = array[j++];
  }

  for (i = start; i <= end; i++) {
    array[i] = arrayIntermediar[i];
  }
}

template <typename T>
void
sort<T>::flip(T* array, std::uint32_t i, std::uint32_t j)
{
  if (array[i] > array[j]) {
    T m = array[i];
    array[i] = array[j];
    array[j] = m;
  }
}

template <typename T>
void
sort<T>::quick(T* array, std::uint32_t startIndex, std::uint32_t endIndex)
{
  if (startIndex < endIndex) {
    auto p = sort<T>::partitionare(array, startIndex, endIndex);
    sort<T>::quick(array, startIndex, p);
    sort<T>::quick(array, p + 1, endIndex);
  }
}
template <typename T>
std::uint32_t
sort<T>::partitionare(T* array, std::uint32_t low, std::uint32_t high)
{
  T pivot = array[(low + high) / 2];
  long i = low;
  long j = high;

  i--;
  j++;

  while (true) {
    do {
      i++;
    } while (array[i] < pivot);

    do {
      j--;
    } while (array[j] > pivot);

    if (i >= j) {
      return j;
    }
    sort<T>::flip(array, i, j);
  }
}

template <typename T>
T* sort<T>::arrayIntermediar = nullptr;

template class sort<int>;
template class sort<double>;
template class sort<float>;
template class sort<char>;
