#ifndef sort_hpp
#define sort_hpp

#include <iostream>
#include <random>
#include <typeinfo>

template <typename T>
class sort
{
private:
  static T* arrayIntermediar;
  static void flip(T* array, std::uint32_t i, std::uint32_t j);
  static void interclasare(T* array, std::uint32_t start, std::uint32_t mid,
                           std::uint32_t end);

  static std::uint32_t partitionare(T* array, std::uint32_t low,
                                    std::uint32_t high);

  static void mergeSort(T* array, std::uint32_t startIndex,
                        std::uint32_t endIndex);
  sort() {}
public:
  static void quick(T* array, std::uint32_t startIndex, std::uint32_t endIndex);
  static void merge(T* array, std::uint32_t startIndex, std::uint32_t endIndex);
};

template <typename T, typename S>
void
randomize(T** array, S size)
{
  std::random_device gen;
  std::mt19937 twister(gen());
  if ((*array) == nullptr) {
    *array = new T[size];
  }
  if (typeid(T) == typeid(int)) {
    std::uniform_int_distribution<int> genINT(INT32_MIN + 1, INT32_MAX - 1);
    for (decltype(size) i = 0; i < size; i++) {
      (*array)[i] = genINT(twister);
    }
  } else if (typeid(T) == typeid(double)) {
    std::uniform_real_distribution<double> genDOUBLE(INT32_MIN + 1,
                                                     INT32_MAX - 1);
    for (decltype(size) i = 0; i < size; i++) {
      (*array)[i] = genDOUBLE(twister);
    }
  } else {
    std::cerr
      << "Deocamdata doar double si int."
         "Daca ai pasat un pointer neinitializat trebuie sa il stergi singur"
      << std::endl;
  }
}

#endif /* sort_hpp */
