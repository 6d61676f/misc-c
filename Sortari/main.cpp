#include <sort.hpp>
#include <algorithm>
#include <chrono>
#include <vector>

int
main(void)
{

  std::uint32_t marimeVector = UINT16_MAX * 100;

  int* vector = nullptr;
  double* vectorD = nullptr;
  std::vector<int> vectorInt;
  std::vector<double> vectorDouble;

  randomize<int>(&vector, marimeVector);
  vectorInt.assign(vector, vector + (marimeVector - 1));

  auto acum = std::chrono::high_resolution_clock::now();
  std::sort(vectorInt.begin(), vectorInt.end(), std::greater<int>());
  auto dupa = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double> intervalSTL = (dupa - acum);

  randomize<int>(&vector, marimeVector);

  acum = std::chrono::high_resolution_clock::now();
  sort<int>::merge(vector, 0, marimeVector - 1);
  dupa = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double> intervalMerge = (dupa - acum);

  std::cout << "\nstd::sort dureaza: " << intervalSTL.count()
            << "\nmergeSort dureaza: " << intervalMerge.count()
            << "\nMergeSort este cu "
            << (intervalSTL.count() - intervalMerge.count()) /
                 intervalMerge.count() * 100
            << " % mai rapid pentru " << marimeVector
            << " de elemente numere intregi" << std::endl;

  randomize<double>(&vectorD, marimeVector);
  vectorDouble.assign(vectorD, vectorD + (marimeVector - 1));

  acum = std::chrono::high_resolution_clock::now();
  std::sort(vectorDouble.begin(), vectorDouble.end(), std::greater<double>());
  dupa = std::chrono::high_resolution_clock::now();
  intervalSTL = (dupa - acum);

  randomize<double>(&vectorD, marimeVector);

  acum = std::chrono::high_resolution_clock::now();
  sort<double>::merge(vectorD, 0, marimeVector - 1);
  dupa = std::chrono::high_resolution_clock::now();
  intervalMerge = (dupa - acum);

  std::cout << "\nstd::sort dureaza: " << intervalSTL.count()
            << "\nmergeSort dureaza: " << intervalMerge.count()
            << "\nMergeSort este cu "
            << (intervalSTL.count() - intervalMerge.count()) /
                 intervalMerge.count() * 100
            << " % mai rapid pentru " << marimeVector
            << " de elemente numere reale" << std::endl;

  randomize<int>(&vector, marimeVector);
  vectorInt.assign(vector, vector + (marimeVector - 1));

  acum = std::chrono::high_resolution_clock::now();
  std::sort(vectorInt.begin(), vectorInt.end(), std::greater<int>());
  dupa = std::chrono::high_resolution_clock::now();
  intervalSTL = (dupa - acum);

  randomize<int>(&vector, marimeVector);

  acum = std::chrono::high_resolution_clock::now();
  sort<int>::merge(vector, 0, marimeVector - 1);
  dupa = std::chrono::high_resolution_clock::now();
  intervalMerge = (dupa - acum);

  std::cout << "\nstd::sort dureaza: " << intervalSTL.count()
            << "\nQuickSort dureaza: " << intervalMerge.count()
            << "\nQuickSort este cu "
            << (intervalSTL.count() - intervalMerge.count()) /
                 intervalMerge.count() * 100
            << " % mai rapid pentru " << marimeVector
            << " de elemente numere intregi" << std::endl;

  randomize<double>(&vectorD, marimeVector);
  vectorDouble.assign(vectorD, vectorD + (marimeVector - 1));

  acum = std::chrono::high_resolution_clock::now();
  std::sort(vectorDouble.begin(), vectorDouble.end(), std::greater<double>());
  dupa = std::chrono::high_resolution_clock::now();
  intervalSTL = (dupa - acum);

  randomize<double>(&vectorD, marimeVector);

  acum = std::chrono::high_resolution_clock::now();
  sort<double>::quick(vectorD, 0, marimeVector - 1);
  dupa = std::chrono::high_resolution_clock::now();
  intervalMerge = (dupa - acum);

  std::cout << "\nstd::sort dureaza: " << intervalSTL.count()
            << "\nQuickSort dureaza: " << intervalMerge.count()
            << "\nQuickSort este cu "
            << (intervalSTL.count() - intervalMerge.count()) /
                 intervalMerge.count() * 100
            << " % mai rapid pentru " << marimeVector
            << " de elemente numere reale" << std::endl;

  randomize<int>(&vector, marimeVector);

  acum = std::chrono::high_resolution_clock::now();
  sort<int>::merge(vector, 0, marimeVector - 1);
  dupa = std::chrono::high_resolution_clock::now();
  intervalSTL = (dupa - acum);

  randomize<int>(&vector, marimeVector);

  acum = std::chrono::high_resolution_clock::now();
  sort<int>::quick(vector, 0, marimeVector - 1);
  dupa = std::chrono::high_resolution_clock::now();
  intervalMerge = (dupa - acum);

  std::cout << "\nMergeSort dureaza: " << intervalSTL.count()
            << "\nQuickSort dureaza: " << intervalMerge.count()
            << "\nQuickSort este cu "
            << (intervalSTL.count() - intervalMerge.count()) /
                 intervalMerge.count() * 100
            << " % mai rapid pentru " << marimeVector
            << " de elemente numere intregi" << std::endl;

  randomize<double>(&vectorD, marimeVector);

  acum = std::chrono::high_resolution_clock::now();
  sort<double>::merge(vectorD, 0, marimeVector - 1);
  dupa = std::chrono::high_resolution_clock::now();
  intervalSTL = (dupa - acum);

  randomize<double>(&vectorD, marimeVector);

  acum = std::chrono::high_resolution_clock::now();
  sort<double>::quick(vectorD, 0, marimeVector - 1);
  dupa = std::chrono::high_resolution_clock::now();
  intervalMerge = (dupa - acum);

  std::cout << "\nMergeSort dureaza: " << intervalSTL.count()
            << "\nQuickSort dureaza: " << intervalMerge.count()
            << "\nQuickSort este cu "
            << (intervalSTL.count() - intervalMerge.count()) /
                 intervalMerge.count() * 100
            << " % mai rapid pentru " << marimeVector
            << " de elemente numere reale" << std::endl;

  delete[] vector;
  delete[] vectorD;

  return (EXIT_SUCCESS);
}
