#ifndef TERMCOLOR_HPP_
#define TERMCOLOR_HPP_
namespace termcolor {
const char* const bold = "\033[1m";
const char* const reset = "\033[00m";
const char* const dark = "\033[2m";
const char* const underline = "\033[4m";
const char* const blink = "\033[5m";
const char* const reverse = "\033[7m";
const char* const concealed = "\033[8m";
const char* const grey = "\033[30m";
const char* const red = "\033[31m";
const char* const green = "\033[32m";
const char* const yellow = "\033[33m";
const char* const blue = "\033[34m";
const char* const magenta = "\033[35m";
const char* const cyan = "\033[36m";
const char* const white = "\033[37m";
const char* const on_grey = "\033[40m";
const char* const on_red = "\033[41m";
const char* const on_green = "\033[42m";
const char* const on_yellow = "\033[43m";
const char* const on_blue = "\033[44m";
const char* const on_magenta = "\033[45m";
const char* const on_cyan = "\033[46m";
const char* const on_white = "\033[47m";
}
#endif // TERMCOLOR_HPP
