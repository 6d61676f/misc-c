#ifndef DICTIONARY_HPP
#define DICTIONARY_HPP
#include <Entry.hpp>
#include <iostream>
#include <set>
#include <sqlite3.h>
class Dictionary
{
  private:
    sqlite3* db = nullptr;
    void createView(void);
    void internalSearch(const std::string& termen, std::set<Entry>& entrySet);
    std::string lexemStatement =
      "SELECT * FROM lexem where lexem.inflection LIKE ?;";
    std::string lexem_definitionStatement =
      "SELECT distinct definition_id FROM "
      "lexem_definition where "
      "lexem_definition.lexem_id == ?;";
    std::string definitionStatement =
      "SELECT definition,source_id FROM definition where id == ?;";

  public:
    Dictionary() = delete;
    Dictionary(const std::string& cale_dictionar);
    Dictionary(const char* cale_dictionar);
    virtual ~Dictionary();
    std::set<Entry> search(const std::string& termen);
    std::set<Entry> search(const char* termen);
};
#endif
