#ifndef ENTRY_HPP
#define ENTRY_HPP
#include <iostream>
#include <map>
class Entry
{
private:
  std::string _lexem;
  std::string _raw_definition;
  std::string _formatted_definition;
  std::uint64_t _lexem_id;
  std::uint64_t _definition_id;
  std::uint64_t _source_id;
  std::map<char, std::string> tagMap;
  std::map<char, bool> tagSet;
  void initMap(
    std::map<char, std::string> valori = std::map<char, std::string>());

public:
  Entry();
  Entry(std::string& lexem, std::string& rawDef, std::uint64_t& lexemId,
        std::uint64_t& defId, std::uint64_t& srcId);
  void setLexem(std::string& lexem);
  void setRawDef(std::string& rawdef);
  void setLexemId(std::uint64_t& lexemId);
  void setDefId(std::uint64_t& defid);
  void setSourceId(std::uint64_t& srcid);
  bool operator==(const Entry& other) const;
  bool operator<(const Entry& other) const;
  friend std::ostream& operator<<(std::ostream& out, const Entry& entry);
  bool colorize(
    std::map<char, std::string> valori = std::map<char, std::string>());
};

#endif
