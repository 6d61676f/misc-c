#include <Dictionary.hpp>
#include <Entry.hpp>
#include <Termcolor.hpp>
#include <iostream>

int
main(int argc, char* argv[])
{
    if (argc < 2) {
        std::cerr << argv[0] << " word1 word2 word3..." << std::endl;
        return (1);
    }
    try {
        Dictionary dict("/home/sarpedon/Downloads/dex.dict");
        for (auto i = 0; i < argc; i++) {
            std::set<Entry>&& intrari = dict.search(argv[i]);
            for (auto entry : intrari) {
                entry.colorize();
                std::cout << i << std::endl;
                std::cout << termcolor::red << std::string(20, '-')
                          << termcolor::reset << std::endl;
            }

            std::cout << "\n\n";
        }
    } catch (std::exception& ex) {
        std::cerr << ex.what() << std::endl;
        return (1);
    }
    return (0);
}
