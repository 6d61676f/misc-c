#include <Dictionary.hpp>
#include <sstream>
#include <stdexcept>
Dictionary::Dictionary(const char* cale_dictionar)
{
    if (cale_dictionar != nullptr) {
        int status = sqlite3_open_v2(cale_dictionar, &this->db,
                                     SQLITE_OPEN_READONLY, nullptr);
        // this->createView();
        if (status != SQLITE_OK) {
            std::string error = sqlite3_errmsg(this->db);
            sqlite3_close_v2(this->db);
            throw std::invalid_argument(error);
        }
    }
}
Dictionary::Dictionary(const std::string& cale_dictionar)
{
    int status = sqlite3_open_v2(cale_dictionar.c_str(), &this->db,
                                 SQLITE_OPEN_READONLY, nullptr);
    // this->createView();
    if (status != SQLITE_OK) {
        std::string error = sqlite3_errmsg(this->db);
        sqlite3_close_v2(this->db);
        throw std::invalid_argument(error);
    }
}
Dictionary::~Dictionary()
{
    sqlite3_close_v2(this->db);
}
std::set<Entry>
Dictionary::search(const std::string& termen)
{
    std::set<Entry> entrySet;
    this->internalSearch(termen, entrySet);
    return (entrySet);
}
std::set<Entry>
Dictionary::search(const char* termen)
{
    std::set<Entry> entrySet;
    std::string sTermen(termen);
    this->internalSearch(sTermen, entrySet);
    return (entrySet);
}
void
Dictionary::internalSearch(const std::string& termen, std::set<Entry>& entrySet)
{
    entrySet.clear();
    sqlite3_stmt* statement = nullptr;
    sqlite3_stmt* defIDStmt = nullptr;
    sqlite3_stmt* defStmt = nullptr;
    if (sqlite3_prepare_v2(this->db, this->lexemStatement.c_str(),
                           this->lexemStatement.size(), &statement,
                           nullptr) != SQLITE_OK) {
        throw std::range_error(sqlite3_errmsg(this->db));
    } else {
        // Lista de inflexiuni
        sqlite3_bind_text(statement, 1, termen.c_str(), termen.size(),
                          SQLITE_STATIC);
        while (sqlite3_step(statement) == SQLITE_ROW) {
            std::string lexem =
              reinterpret_cast<const char*>(sqlite3_column_text(statement, 1));
            std::uint64_t lexem_id = sqlite3_column_int(statement, 0);
            if (sqlite3_prepare_v2(this->db,
                                   this->lexem_definitionStatement.c_str(),
                                   this->lexem_definitionStatement.size(),
                                   &defIDStmt, nullptr) != SQLITE_OK) {
                throw std::range_error(sqlite3_errmsg(this->db));
            } else {
                // Lista de id-uri ale definitiilor
                sqlite3_bind_int(defIDStmt, 1, lexem_id);
                while (sqlite3_step(defIDStmt) == SQLITE_ROW) {
                    std::uint64_t defif = sqlite3_column_int(defIDStmt, 0);
                    if (sqlite3_prepare_v2(this->db,
                                           this->definitionStatement.c_str(),
                                           this->definitionStatement.size(),
                                           &defStmt, nullptr) != SQLITE_OK) {
                        throw std::range_error(sqlite3_errmsg(this->db));
                    } else {
                        // Lista de definitii
                        sqlite3_bind_int64(defStmt, 1, defif);
                        while (sqlite3_step(defStmt) == SQLITE_ROW) {
                            std::string definitie =
                              reinterpret_cast<const char*>(
                                sqlite3_column_text(defStmt, 0));
                            std::uint64_t sourceId =
                              sqlite3_column_int(defStmt, 1);
                            entrySet.insert(Entry(lexem, definitie, lexem_id,
                                                  defif, sourceId));
                        }
                        sqlite3_finalize(defStmt);
                    }
                }
                sqlite3_finalize(defIDStmt);
            }
        }
        sqlite3_finalize(statement);
    }
}
