#include <Entry.hpp>
#include <Termcolor.hpp>
#include <iostream>
#include <map>
#include <sstream>
void
Entry::setLexem(std::string& lexem)
{
  this->_lexem = lexem;
}
void
Entry::setRawDef(std::string& rawdef)
{
  this->_raw_definition = rawdef;
}
void
Entry::setLexemId(std::uint64_t& lexemId)
{
  this->_lexem_id = lexemId;
}
void
Entry::setDefId(std::uint64_t& defid)
{
  this->_definition_id = defid;
}
void
Entry::setSourceId(std::uint64_t& srcid)
{
  this->_source_id = srcid;
}
bool
Entry::operator==(const Entry& other) const
{
  return (this->_lexem_id == other._lexem_id &&
          this->_definition_id == other._definition_id);
}
bool
Entry::operator<(const Entry& other) const
{
  if (this->_definition_id < other._definition_id) {
    return (true);
  } else if (this->_lexem_id < other._lexem_id) {
    return (true);
  } else {
    return (false);
  }
}
Entry::Entry(std::string& lexem,
             std::string& rawDef,
             std::uint64_t& lexemId,
             std::uint64_t& defId,
             std::uint64_t& srcId)
{
  this->_lexem = lexem;
  this->_raw_definition = rawDef;
  this->_lexem_id = lexemId;
  this->_definition_id = defId;
  this->_source_id = srcId;
  this->initMap();
}
std::ostream&
operator<<(std::ostream& out, const Entry& entry)
{
  if (entry._formatted_definition.length() == 0) {
    out << entry._raw_definition;
  } else {
    out << entry._formatted_definition;
  }
  return (out);
}
void
Entry::initMap(std::map<char, std::string> valori)
{
#ifndef TERMCOLOR_HPP_
  this->tagMap.insert(std::pair<char, std::string>('@', "\033[32m"));
  this->tagMap.insert(std::pair<char, std::string>('$', "\033[36m"));
  this->tagMap.insert(std::pair<char, std::string>('%', "\033[31m"));
  this->tagMap.insert(std::pair<char, std::string>('#', "\033[4m"));
#else
  this->tagMap.insert(std::pair<char, std::string>('@', termcolor::green));
  this->tagMap.insert(std::pair<char, std::string>('$', termcolor::cyan));
  this->tagMap.insert(std::pair<char, std::string>('%', termcolor::red));
  this->tagMap.insert(std::pair<char, std::string>('#', termcolor::underline));
#endif
  this->tagMap.insert(valori.begin(), valori.end());
  for (auto i : this->tagMap) {
    this->tagSet.insert({ i.first, false });
  }
}

bool
Entry::colorize(std::map<char, std::string> valori)
{
  this->initMap(valori);
  auto stringIterator = this->_raw_definition.begin();
  std::ostringstream ss;
  for (; stringIterator != this->_raw_definition.end(); stringIterator++) {
    const auto& tagMapIt = this->tagMap.find(*stringIterator);
    if (tagMapIt != this->tagMap.end()) {
      const auto& tagSetIt = tagSet.find(*stringIterator);
      if (tagSetIt->second) {
        ss << "\033[00m";
        tagSetIt->second = false;
      } else {
        stringIterator++;
        if (*stringIterator >= '0' && *stringIterator <= '9') {
          ss << "\n\t";
        }
        stringIterator--;
        ss << tagMapIt->second;
        tagSetIt->second = true;
      }
    } else {
      ss << *stringIterator;
    }
  }
  if (this->_formatted_definition == ss.str()) {
    return false;
  } else {
    this->_formatted_definition = ss.str();
    return true;
  }
}
