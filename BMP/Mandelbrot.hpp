#ifndef MANDELBROT_HPP
#define MANDELBROT_HPP
#include "BMP.hpp"
#include <iostream>
class Mandelbrot
{
private:
  BMP& _bmp;
  uint32_t _width{ 0 };
  uint32_t _height{ 0 };
  double _xCenter{ 0 };
  double _yCenter{ 0 };
  double _scale{ 1.0 };
  uint32_t _max_iterations{ 0 };
  std::unique_ptr<uint32_t[]> _histogram;
  std::unique_ptr<uint32_t[]> _fractal;
  uint64_t _total{ 0 };
  double _hue{ 0.0 };
  uint32_t getIteration(double x, double y);
  double transformX(uint32_t x);
  double transformY(uint32_t y);

public:
  BMP& getBitmap();
  void centerOn(uint32_t xCenter, uint32_t yCenter, double scale);
  Mandelbrot(BMP& bmp, uint32_t max_iterations);
  Mandelbrot() = delete;
  void populateBitmap();
  void resetCentering();
  virtual ~Mandelbrot();
};

#endif
