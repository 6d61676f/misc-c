#include "Mandelbrot.hpp"
#include <complex>

Mandelbrot::Mandelbrot(BMP& bmp, uint32_t max_iterations)
  : _bmp(bmp)
  , _width(bmp.getWidth())
  , _height(bmp.getHeight())
  , _xCenter(0)
  , _yCenter(0)
  , _scale(4.0 / _width)
  , _max_iterations(max_iterations)
  , _histogram(new uint32_t[max_iterations + 1]())
  , _fractal(new uint32_t[_width * _height * 3]())
{}
Mandelbrot::~Mandelbrot() {}
double
Mandelbrot::transformX(uint32_t x)
{
  return (((double)x - (double)_width / 2) * _scale + _xCenter);
}
double
Mandelbrot::transformY(uint32_t y)
{
  return (((double)y - (double)_height / 2) * _scale + _yCenter);
}
uint32_t
Mandelbrot::getIteration(double x, double y)
{
  uint32_t iteration = 0;
  double a{ 0 }, b{ 0 };
  //   std::complex<double> a = 0;
  //   std::complex<double> b(x, y);
  //   while (iteration < _max_iterations && std::abs(a) < 2) {
  //       a = a * a + b;
  //       iteration++;
  //   }
  while (iteration < _max_iterations && a * a + b * b < 4) {
    iteration++;
    double temp = a * a - b * b + x;
    b = 2 * a * b + y;
    a = temp;
  }
  return iteration;
}
void
Mandelbrot::populateBitmap()
{
  std::fill(
    this->_histogram.get(), this->_histogram.get() + _max_iterations + 1, 0);
  std::fill(
    this->_fractal.get(), this->_fractal.get() + _width * _height * 3, 0);
  uint32_t iteration{ 0 };
  double color{ 0 }, red{ 0 }, green{ 0 }, blue{ 0 };
  for (uint32_t i = 0; i < _width; i++) {
    std::cout << i << " < " << _width << std::endl;
    double x0 = transformX(i);
    for (uint32_t j = 0; j < _height; j++) {
      double y0 = transformY(j);
      iteration = this->getIteration(x0, y0);
      if (iteration != _max_iterations) {
        _histogram[iteration]++;
        _fractal[j * _width + i] = iteration;
      }
    }
  }
  for (uint32_t i = 0; i < _max_iterations; i++) {
    _total += _histogram[i];
  }
  for (uint32_t i = 0; i < _max_iterations; i++) {
    _hue += (double)_histogram[i] / _total;
  }
  for (uint32_t i = 0; i < _width; i++) {
    for (uint32_t j = 0; j < _height; j++) {
      color = _fractal[j * _width + i] * 2.7182818284590452 * _hue;
      red = color * 1.51;
      green = color * 2.71;
      blue = color * 3.14;
      _bmp.setPixel(i, j, red, green, blue);
    }
  }
}
BMP&
Mandelbrot::getBitmap()
{
  return this->_bmp;
}
void
Mandelbrot::centerOn(uint32_t xCenter, uint32_t yCenter, double scale)
{
  _xCenter += ((double)xCenter - _width / 2) * _scale;
  _yCenter += (_height / 2 - (double)yCenter) * _scale;
  _scale *= scale;
}
void
Mandelbrot::resetCentering()
{
  this->_xCenter = 0.0;
  this->_yCenter = 0.0;
  this->_scale = 4.0 / this->_width;
}
