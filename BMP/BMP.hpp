#ifndef BMP_HPP
#define BMP_HPP
#include "BitmapHeader.h"
#include <cstdint>
#include <iostream>
#include <memory>
class BMP
{
private:
  std::unique_ptr<uint8_t> memory{ nullptr };
  struct bmpHeader header;

public:
  BMP() = default;
  BMP(uint32_t width, uint32_t height);
  void setPixel(uint32_t x,
                uint32_t y,
                uint8_t red,
                uint8_t green,
                uint8_t blue);
  virtual ~BMP();
  bool write(const std::string& path);
  uint32_t getWidth();
  uint32_t getHeight();
};

#endif
