#ifndef BITMAP_HEADER_HPP
#define BITMAP_HEADER_HPP
#include <cstdint>
#pragma pack(push, 2)
struct bmpHeader
{
    const uint16_t signature{ 0x4D42 };
    uint32_t totalSize;
    uint16_t reserved[2];
    uint32_t offset;
    const uint32_t sizeOfInfoHeader{ 40 };
    uint32_t width;
    uint32_t height;
    const uint16_t planes{ 1 };
    uint16_t bitDepth{ 24 };
    uint32_t compression{ 0 };
    uint32_t imageSize;
    uint32_t horizontalPixelPerMeter{ 2400 };
    uint32_t verticalPixelPerMeter{ 2400 };
    uint32_t numberOfColors;
    uint32_t numberOfImportantColors;
};
#pragma pack(pop)

#endif
