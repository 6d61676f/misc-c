#include "BMP.hpp"
#include <fstream>
#include <iostream>
BMP::BMP(uint32_t width, uint32_t height)
  : memory(new uint8_t[width * height * 3])
{
  this->header.width = width;
  this->header.height = height;
  this->header.imageSize = width * height * 3;
  this->header.totalSize =
    this->header.imageSize + this->header.sizeOfInfoHeader + 14;
}
BMP::~BMP() {}
bool
BMP::write(const std::string& path)
{
  std::ofstream file(path, std::ios::binary | std::ios::out);
  if (!file.is_open()) {
    return false;
  } else {
    file.write(reinterpret_cast<const char*>(&this->header),
               sizeof this->header);
    file.write(reinterpret_cast<const char*>(this->memory.get()),
               this->header.imageSize);
    file.close();
    if (file.is_open()) {
      return false;
    }
  }
  return true;
}
void
BMP::setPixel(uint32_t x, uint32_t y, uint8_t red, uint8_t green, uint8_t blue)
{
  uint8_t* pPixel = this->memory.get();
  pPixel += (y * 3) * this->header.width + (x * 3);
  // Little endian
  pPixel[2] = red;
  pPixel[1] = green;
  pPixel[0] = blue;
}
uint32_t
BMP::getWidth()
{
  return this->header.width;
}
uint32_t
BMP::getHeight()
{
  return this->header.height;
}
