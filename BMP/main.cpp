#include "BMP.hpp"
#include "BitmapHeader.h"
#include "Mandelbrot.hpp"
#include <iostream>
int
main(void)
{
  // Max
  // const int WIDTH = 30720;
  // const int HEIGHT = 17280;
  try {
    const int WIDTH{ 3840 };
    const int HEIGHT{ 2160 };
    const uint16_t MAX_ITERATION{ 1000 };
    BMP bmp(WIDTH, HEIGHT);
    Mandelbrot bro(bmp, MAX_ITERATION);
    bro.populateBitmap();
    bmp.write("/tmp/poza.bmp");
    /* Zoom in */
    bro.centerOn(1834, 214, 0.5);
    bro.populateBitmap();
    bmp.write("/tmp/poza-zoom.bmp");
    bro.resetCentering();
    bro.centerOn(1834, 214, 0.1);
    bro.populateBitmap();
    bmp.write("/tmp/poza-zoom1.bmp");
  } catch (std::exception& ex) {
    std::cerr << ex.what() << std::endl;
    return 1;
  }
  return 0;
}
