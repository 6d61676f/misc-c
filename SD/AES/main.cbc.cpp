#include <algorithm>
#include <cstring>
#include <fstream>
#include <iostream>
#include <openssl/aes.h>
#define CHUNK AES_BLOCK_SIZE
int
main(int argc, char* argv[])
{
  std::ifstream in;
  std::ofstream out;

  if (argc == 3) {
    uint8_t bufferIn[CHUNK]{ 0x0 };
    uint8_t bufferOut[CHUNK]{ 0x0 };
    uint8_t passwd[AES_BLOCK_SIZE]{ 1, 2, 3, 4, 5, 6, 7, 8,
                                    9, 0, 1, 2, 3, 4, 5, 6 };
    uint8_t iv[AES_BLOCK_SIZE]{
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    };
    AES_KEY key;
    size_t bytes{ 0 };
    size_t originalFileSize{ 0 };
    size_t printOriginal{ 0 };
    in.open(argv[1], std::ios::binary | std::ios::in | std::ios::ate);
    originalFileSize = in.tellg();
    in.seekg(0, in.beg);
    out.open(argv[2], std::ios::binary | std::ios::out);
    out.write((char*)&originalFileSize, sizeof originalFileSize);
    AES_set_encrypt_key(passwd, AES_BLOCK_SIZE * 8, &key);
    while ((bytes = in.readsome((char*)bufferIn, CHUNK)) != 0) {
      AES_cbc_encrypt(bufferIn, bufferOut, bytes, &key, iv, AES_ENCRYPT);
      out.write((char*)bufferOut, CHUNK);
    }
    in.close();
    out.close();

    in.open(argv[2], std::ios::binary | std::ios::in);
    in.read((char*)&originalFileSize, sizeof originalFileSize);
    std::fill(iv, iv + AES_BLOCK_SIZE, 0x0);
    AES_set_decrypt_key(passwd, AES_BLOCK_SIZE * 8, &key);
    while ((bytes = in.readsome((char*)bufferIn, CHUNK)) != 0) {
      AES_cbc_encrypt(bufferIn, bufferOut, bytes, &key, iv, AES_DECRYPT);
      for (size_t i = 0; printOriginal < originalFileSize && i < bytes;
           i++, printOriginal++) {
        std::cout << bufferOut[i];
      }
    }
    std::cout << std::endl;
    in.close();
  } else {
    std::cerr << argv[0] << " in out" << std::endl;
    return (-1);
  }
}
