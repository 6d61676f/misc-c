#include <fstream>
#include <iostream>
#include <openssl/aes.h>
#define CHUNK AES_BLOCK_SIZE
int
main(int argc, char* argv[])
{
  std::ifstream in;
  std::ofstream out;
  if (argc == 3) {
    uint8_t bufferIn[CHUNK]{ 0 };
    uint8_t bufferOut[CHUNK]{ 0 };
    uint8_t passwd[CHUNK]{ 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6 };
    size_t bytes{ 0 };
    size_t originalFileSize{ 0 };
    size_t printFile{ 0 };
    AES_KEY key;
    in.open(argv[1], std::ios::in | std::ios::binary | std::ios::ate);
    originalFileSize = in.tellg();
    in.seekg(0, in.beg);
    out.open(argv[2], std::ios::out | std::ios::binary);
    out.write((char*)&originalFileSize, sizeof originalFileSize);
    AES_set_encrypt_key(passwd, AES_BLOCK_SIZE * 8, &key);
    while ((bytes = in.readsome((char*)bufferIn, CHUNK)) != 0) {
      AES_ecb_encrypt(bufferIn, bufferOut, &key, AES_ENCRYPT);
      out.write((char*)bufferOut, CHUNK);
    }
    in.close();
    out.close();

    in.open(argv[2], std::ios::in | std::ios::binary);
    in.read((char*)&originalFileSize, sizeof originalFileSize);
    AES_set_decrypt_key(passwd, AES_BLOCK_SIZE * 8, &key);
    while ((bytes = in.readsome((char*)bufferIn, CHUNK)) != 0) {
      AES_ecb_encrypt(bufferIn, bufferOut, &key, AES_DECRYPT);
      for (size_t i = 0; printFile < originalFileSize && i < bytes;
           printFile++, i++) {
        std::cout << bufferOut[i];
      }
    }
    in.close();
    std::cout << std::endl;
  } else {
    std::cerr << argv[0] << " in out" << std::endl;
    return -1;
  }
}
