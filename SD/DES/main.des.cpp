#include <algorithm>
#include <fstream>
#include <iostream>
#include <openssl/des.h>
#define CHUNK 64

int
main(int argc, char* argv[])
{
  std::ifstream in;
  std::ofstream out;
  if (argc != 3) {
    std::cerr << argv[0] << " in out\n";
    return -1;
  } else {
    std::uint8_t message[CHUNK], cyphertext[CHUNK];
    std::uint8_t passwd[DES_KEY_SZ]{ 1, 2, 3, 4, 5, 6, 7, 8 };
    std::size_t readBytes{ 0 };
    int val{ 0 };
    DES_cblock desKey;
    DES_key_schedule desSchedule;
    // Encryption
    in.open(argv[1], std::ios::binary | std::ios::in);
    out.open(argv[2], std::ios::binary | std::ios::out);
    std::copy(passwd, passwd + DES_KEY_SZ, desKey);
    DES_set_odd_parity(&desKey);
    if (DES_set_key_checked(&desKey, &desSchedule) < 0) {
      std::cerr << "Error set key checked";
      return -1;
    }
    while ((readBytes = in.readsome((char*)message, CHUNK)) != 0) {
      DES_cfb64_encrypt(message,
                        cyphertext,
                        readBytes,
                        &desSchedule,
                        &desKey,
                        &val,
                        DES_ENCRYPT);
      out.write((char*)cyphertext, readBytes);
    }
    in.close();
    out.close();

    // Decryption
    in.open(argv[2], std::ios::in | std::ios::binary);
    std::copy(passwd, passwd + DES_KEY_SZ, desKey);
    DES_set_odd_parity(&desKey);
    if (DES_set_key_checked(&desKey, &desSchedule) < 0) {
      std::cerr << "Problem";
      return -1;
    }
    // Cacatu asta trebuie sa fie zero!!!
    val = 0;
    // Beware
    while ((readBytes = in.readsome((char*)cyphertext, CHUNK)) != 0) {
      DES_cfb64_encrypt(cyphertext,
                        message,
                        readBytes,
                        &desSchedule,
                        &desKey,
                        &val,
                        DES_DECRYPT);
      for (size_t i = 0; i < readBytes; i++) {
        std::cout << message[i];
      }
    }
    std::cout << std::endl;
    in.close();
    return 0;
  }
}
