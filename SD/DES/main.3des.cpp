#include <algorithm>
#include <fstream>
#include <iostream>
#include <openssl/des.h>
#define CHUNK 64
int
main(int argc, char* argv[])
{
  std::ifstream in;
  std::ofstream out;

  if (argc == 3) {
    int val{ 0 };
    std::uint8_t message[CHUNK], cyphertext[CHUNK];
    std::size_t readBytes{ 0 };
    std::uint8_t passwd[DES_KEY_SZ] = { 1, 2, 3, 4, 5, 6, 7, 8 };
    DES_cblock cblk1 = { 0xAE, 0xAE, 0xAE, 0xAE, 0xAE, 0xAE, 0xAE, 0xAE };
    DES_cblock cblk2 = { 0xAE, 0xAE, 0xAE, 0xAE, 0xAE, 0xAE, 0xAE, 0xAE };
    DES_cblock cblk3 = { 0xAE, 0xAE, 0xAE, 0xAE, 0xAE, 0xAE, 0xAE, 0xAE };
    DES_key_schedule sch1, sch2, sch3;
    DES_cblock desKey;
    // Encrypt
    in.open(argv[1], std::ios::binary | std::ios::in);
    out.open(argv[2], std::ios::binary | std::ios::out);
    std::copy(passwd, passwd + DES_KEY_SZ, desKey);
    DES_set_odd_parity(&desKey);
    if (DES_set_key_checked(&cblk1, &sch1) ||
        DES_set_key_checked(&cblk2, &sch2) ||
        DES_set_key_checked(&cblk3, &sch3)) {
      std::cerr << "DES_set_key_checked fucked up at encrypt" << std::endl;
      return (-1);
    }
    while ((readBytes = in.readsome((char*)message, CHUNK)) != 0) {
      DES_ede3_cfb64_encrypt(message,
                             cyphertext,
                             readBytes,
                             &sch1,
                             &sch2,
                             &sch3,
                             &desKey,
                             &val,
                             DES_ENCRYPT);
      out.write((char*)cyphertext, readBytes);
    }

    out.close();
    in.close();

    // Decrypt
    in.open(argv[2], std::ios::binary | std::ios::in);
    std::copy(passwd, passwd + DES_KEY_SZ, desKey);
    // Neaparat val =0
    val = 0;
    // Like rlly!
    DES_set_odd_parity(&desKey);
    if (DES_set_key_checked(&cblk1, &sch1) ||
        DES_set_key_checked(&cblk2, &sch2) ||
        DES_set_key_checked(&cblk3, &sch3)) {
      std::cerr << "DES_set_key_checked fucked up at decrypt" << std::endl;
      return (-1);
    }
    while ((readBytes = in.readsome((char*)cyphertext, CHUNK)) != 0) {
      DES_ede3_cfb64_encrypt(cyphertext,
                             message,
                             readBytes,
                             &sch1,
                             &sch2,
                             &sch3,
                             &desKey,
                             &val,
                             DES_DECRYPT);
      for (size_t i = 0; i < readBytes; i++) {
        std::cout << message[i];
      }
    }
    std::cout << std::endl;
    in.close();
    return 0;

  } else {
    std::cerr << argv[0] << " in out" << std::endl;
    return (-1);
  }
}
