#include <algorithm>
#include <iomanip>
#include <iostream>
#include <vector>

int
main(int argc, char* argv[])
{
  if (argc < 2) {
    std::cerr << argv[0] << " max_number\n";
    return EXIT_FAILURE;
  }

  const size_t maxNumber = strtoul(argv[1], nullptr, 10);
  std::vector<bool> primes(maxNumber);
  uint_fast8_t width = 1;

  {
    size_t maxTempNumber = maxNumber;
    while (maxTempNumber /= 10) {
      width++;
    }
  }

  size_t current = 2;
  while (current * current < maxNumber) {
    if (primes[current] == false) {
      for (size_t i = current; i * current < maxNumber; i++) {
        primes[i * current] = true;
      }
    }
    current++;
  }

  uint_fast8_t elemsPerRow = 0;
  for (size_t i = 2; i < maxNumber; i++) {
    if (primes[i] == false) {
      std::cout << std::setw(width) << i << " ";
      if (++elemsPerRow == 5) {
        std::cout << "\n";
        elemsPerRow = 0;
      }
    }
  }
  std::cout << "\n\nWe have a total of "
            << std::count_if(primes.begin() + 2,
                             primes.end(),
                             [](bool val) { return !val; })
            << " primes before " << maxNumber << "\n";

  return std::cout << "\n", EXIT_SUCCESS;
}
