// CHEILE TREBUIE SA FIE PE 2048 de biti cu 256 BIGNUM
#include <iostream>
#include <memory>
#include <openssl/pem.h>
#include <openssl/rsa.h>
#include <openssl/sha.h>

using RSA_ptr = std::unique_ptr<RSA, decltype(&::RSA_free)>;
using FILE_ptr = std::unique_ptr<FILE, decltype(&::fclose)>;

int
main(int argc, char* argv[])
{
    if (argc != 4) {
        std::cerr << argv[0] << " pubKey signedHashFile originalFile\n";
        return 1;
    } else {
        FILE_ptr fKey(fopen(argv[1], "rb"), fclose);
        FILE_ptr fHash(fopen(argv[2], "rb"), fclose);
        FILE_ptr fIn(fopen(argv[3], "rb"), fclose);
        RSA* rsa = RSA_new();
        RSA_ptr rsaUniq(nullptr, RSA_free);
        if (fKey.get() && fHash.get() && fIn.get()) {
            size_t rsaSize, readSize;
            PEM_read_RSAPublicKey(fKey.get(), &rsa, nullptr, nullptr);
            rsaSize = RSA_size(rsa);
            rsaUniq = RSA_ptr(rsa, RSA_free);

            uint8_t inBuff[SHA_DIGEST_LENGTH], md[SHA_DIGEST_LENGTH],
              signedHash[rsaSize];

            SHA_CTX shaIns;
            SHA1_Init(&shaIns);
            while ((readSize =
                      fread(inBuff, 1, SHA_DIGEST_LENGTH, fIn.get())) != 0) {
                SHA1_Update(&shaIns, inBuff, readSize);
            }
            SHA1_Final(md, &shaIns);

            fread(signedHash, 1, rsaSize, fHash.get());
            // RSA_public_decrypt(rsaSize, signedHash, md, rsaUniq.get(),
            //                   RSA_PKCS1_PADDING);
            for (size_t i = 0; i < SHA_DIGEST_LENGTH; i++) {
                std::cout << std::hex << (unsigned)md[i];
            }
            if (RSA_verify(NID_sha1, md, SHA_DIGEST_LENGTH, signedHash, rsaSize,
                           rsaUniq.get())) {
                std::cout << "\nIt checks out\n";
            } else {
                std::cout << "\nIt does not check out\n";
            }
            return (0);
        }
        return (1);
    }
}
