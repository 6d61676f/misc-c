// CHEILE TREBUIE SA FIE PE 2048 de biti cu 256 BIGNUM
#include <openssl/pem.h>
#include <openssl/rsa.h>
#include <openssl/sha.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#define CHUNK SHA_DIGEST_LENGTH

int
main(int argc, char* argv[])
{
    if (argc != 3) {
        fprintf(stderr, "%s key file\n", argv[0]);
        return (1);
    } else {
        size_t destNameSize = strlen(argv[2]) + strlen(".signed") + 1;
        char* nume = calloc(1, destNameSize);
        strncpy(nume, argv[2], strlen(argv[2]));
        strncat(nume, ".signed", destNameSize - 1);
        FILE* out = fopen(nume, "wb");
        unsigned sigType = 0;

        FILE* key = fopen(argv[1], "rb");
        RSA* rsa = RSA_new();
        PEM_read_RSAPrivateKey(key, &rsa, NULL, NULL);
        fclose(key);
        size_t rsaSize = RSA_size(rsa);
        uint8_t* signedHash = calloc(1, rsaSize);

        FILE* fis = fopen(argv[2], "rb");
        SHA_CTX mdInstance;
        SHA1_Init(&mdInstance);
        uint8_t in[CHUNK], md[SHA_DIGEST_LENGTH];
        size_t readSize = 0;

        while ((readSize = fread(in, 1, CHUNK, fis)) != 0) {
            SHA1_Update(&mdInstance, in, readSize);
        }
        fclose(fis);
        SHA1_Final(md, &mdInstance);

        // RSA_private_encrypt(SHA_DIGEST_LENGTH, md, signedHash, rsa,
        //                    RSA_PKCS1_PADDING);

        RSA_sign(NID_sha1, md, SHA_DIGEST_LENGTH, signedHash, &sigType, rsa);

        fwrite(signedHash, 1, rsaSize, out);

        RSA_free(rsa);
        fclose(out);
        free(signedHash);
        free(nume);

        return (0);
    }

    return (0);
}
