// CHEILE TREBUIE SA FIE PE 2048 de biti cu 256 BIGNUM
#include <iostream>
#include <memory>
#include <openssl/pem.h>
#include <openssl/rsa.h>
#include <openssl/sha.h>

using RSA_ptr = std::unique_ptr<RSA, decltype(&::RSA_free)>;
using FILE_ptr = std::unique_ptr<FILE, decltype(&::fclose)>;

int
main(int argc, char* argv[])
{
    if (argc != 4) {
        std::cerr << argv[0] << " privKey inFile outFile\n";
        return 1;
    } else {
        FILE_ptr fKey(fopen(argv[1], "rb"), fclose);
        FILE_ptr fIn(fopen(argv[2], "rb"), fclose);
        FILE_ptr fOut(fopen(argv[3], "wb"), fclose);
        RSA* rsa = RSA_new();
        RSA_ptr rsaUniq(nullptr, RSA_free);
        if (fKey.get() && fIn.get() && fOut.get()) {
            PEM_read_RSAPrivateKey(fKey.get(), &rsa, nullptr, nullptr);
            rsaUniq = RSA_ptr(rsa, RSA_free);
            size_t rsaSize = RSA_size(rsaUniq.get()), readSize{ 0 },
                   sigLen{ 0 };

            SHA_CTX shaIns;
            SHA1_Init(&shaIns);

            std::uint8_t readBuf[SHA_DIGEST_LENGTH], md[SHA_DIGEST_LENGTH],
              signedHash[rsaSize];

            while ((readSize =
                      fread(readBuf, 1, SHA_DIGEST_LENGTH, fIn.get())) != 0) {
                SHA1_Update(&shaIns, readBuf, readSize);
            }
            SHA1_Final(md, &shaIns);
            for (size_t i = 0; i < SHA_DIGEST_LENGTH; i++) {
                std::cout << std::hex << (unsigned)md[i];
            }

            RSA_sign(NID_sha1, md, SHA_DIGEST_LENGTH, signedHash,
                     (unsigned int*)&sigLen, rsaUniq.get());
            fwrite(signedHash, 1, rsaSize, fOut.get());
            return (0);
        }
    }
}
