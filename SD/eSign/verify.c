// CHEILE TREBUIE SA FIE PE 2048 de biti cu 256 BIGNUM
#include <assert.h>
#include <openssl/pem.h>
#include <openssl/rsa.h>
#include <openssl/sha.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#define CHUNK SHA_DIGEST_LENGTH

int
main(int argc, char* argv[])
{
  if (argc != 3) {
    fprintf(stderr, "%s key file\n", argv[0]);
    return (1);
  } else {
    FILE* keyFile = fopen(argv[1], "rb");
    FILE* hashFile = fopen(argv[2], "rb");
    char* numeOrig = strdup(argv[2]);
    assert(numeOrig);
    FILE* fisOrig = fopen(numeOrig, "rb");
    char* dot = strstr(numeOrig, ".signed");
    uint8_t buff[CHUNK], hash[SHA_DIGEST_LENGTH];
    SHA_CTX mdInstance;
    size_t readSize = 0;
    RSA* rsa = RSA_new();
    size_t rsaSize = RSA_size(rsa);
    uint8_t signedHash[rsaSize];

    if (dot) {
      *dot = '\0';
    } else {
      fprintf(stderr, "Signed file not found\n");
      goto done;
    }

    assert(keyFile != NULL);
    assert(hashFile != NULL);
    assert(fisOrig != NULL);

    SHA1_Init(&mdInstance);

    while ((readSize = fread(buff, 1, CHUNK, fisOrig))) {
      SHA1_Update(&mdInstance, buff, readSize);
    }
    fclose(fisOrig);
    SHA1_Final(hash, &mdInstance);
    for (size_t i = 0; i < SHA_DIGEST_LENGTH; i++) {
      printf("%02x", hash[i]);
    }
    puts("");

    PEM_read_RSAPublicKey(keyFile, &rsa, NULL, NULL);

    fread(signedHash, 1, rsaSize, hashFile);

    if (RSA_verify(
          NID_sha1, hash, SHA_DIGEST_LENGTH, signedHash, rsaSize, rsa) == 1) {
      printf("It checks out\n");
    } else {
      printf("Wtf\n");
    }

  done:
    fclose(keyFile);
    fclose(hashFile);
    RSA_free(rsa);
    free(numeOrig);
  }
}
