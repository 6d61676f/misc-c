#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <openssl/md5.h>
#define CHUNK 64

int
main(int argc, char* argv[])
{

  std::ifstream in;
  if (argc != 2) {
    std::cerr << argv[0] << " fis" << std::endl;
    return -1;
  } else {
    MD5_CTX md5Instance;
    std::uint8_t digest[MD5_DIGEST_LENGTH]{ 0x0 };
    std::uint8_t buffer[CHUNK]{ 0x0 };
    in.open(argv[1], std::ios::in | std::ios::binary);
    if (in) {
      std::size_t readBytes{ 0 };
      MD5_Init(&md5Instance);
      while ((readBytes = in.readsome((char*)buffer, CHUNK)) != 0) {
        MD5_Update(&md5Instance, &buffer, readBytes);
      }
      in.close();
      MD5_Final(digest, &md5Instance);
      std::cout << std::hex << std::setw(2) << std::setfill('0');
      std::for_each(digest, digest + MD5_DIGEST_LENGTH, [](unsigned i) {
        std::cout << i << " ";
      });
      std::cout << std::dec << std::setw(0) << std::endl;
    } else {
      std::cerr << "Nu am putut deschide: " << argv[1] << std::endl;
      return -1;
    }
  }
  return -1;
}
