#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <openssl/sha.h>
#define CHUNK 64
int
main(int argc, char* argv[])
{
  if (argc != 2) {
    std::cerr << argv[0] << " fis" << std::endl;
    return -1;
  } else {
    std::ifstream file(argv[1], std::ios::in | std::ios::binary);
    SHA512_CTX shaInstance;
    std::uint8_t buffer[CHUNK]{ 0x0 };
    std::uint8_t digest[SHA512_DIGEST_LENGTH]{ 0x0 };
    if (file) {
      SHA512_Init(&shaInstance);
      size_t readBytes{ 0 };
      while ((readBytes = file.readsome((char*)buffer, CHUNK)) != 0) {
        SHA512_Update(&shaInstance, buffer, readBytes);
      }
      file.close();
      SHA512_Final(digest, &shaInstance);
      std::cout << std::hex << std::setw(2) << std::setfill('0');
      std::for_each(digest, digest + SHA512_DIGEST_LENGTH, [](unsigned i) {
        std::cout << i << " ";
      });
      std::cout << std::dec << std::setw(0) << std::endl;
      return 0;
    } else {
      std::cerr << "Nu pot deschide fisierul" << std::endl;
      return -1;
    }
  }
}
