#include <assert.h>
#include <openssl/bn.h>
#include <openssl/evp.h>
#include <openssl/pem.h>
#include <openssl/rsa.h>
#include <openssl/x509.h>
#include <stdio.h>

int
main(void)
{
    EVP_PKEY* pkey = EVP_PKEY_new();
    RSA* rsa = RSA_new();
    BIGNUM* bigno = BN_new();
    X509* cert = X509_new();
    FILE* file_rsa = fopen("privX509.pem", "wb");
    FILE* file_x509 = fopen("x509.cert", "wb");

    assert(BN_generate_prime_ex(bigno, 256, 1, NULL, NULL, NULL) == 1);
    assert(RSA_generate_key_ex(rsa, 4096, bigno, NULL) == 1);
    EVP_PKEY_assign_RSA(pkey, rsa);

    ASN1_INTEGER_set(X509_get_serialNumber(cert), 1);
    X509_set_version(cert, 2L);
    X509_gmtime_adj(X509_get_notBefore(cert), 0);
    X509_gmtime_adj(X509_get_notAfter(cert), 60 * 60 * 24 * 365);

    X509_set_pubkey(cert, pkey);

    X509_NAME* name = X509_get_subject_name(cert);

    X509_NAME_add_entry_by_txt(name, "C", MBSTRING_ASC, (unsigned char*)"RO",
                               -1, -1, 0);

    X509_NAME_add_entry_by_txt(name, "O", MBSTRING_ASC, (unsigned char*)"ISM",
                               -1, -1, 0);

    X509_NAME_add_entry_by_txt(name, "CN", MBSTRING_ASC,
                               (unsigned char*)"localhost", -1, -1, 0);

    X509_set_issuer_name(cert, name);
    X509_sign(cert, pkey, EVP_sha512());

    PEM_write_RSAPrivateKey(file_rsa, rsa, NULL, NULL, 0, NULL, NULL);
    PEM_write_X509(file_x509, cert);

    EVP_PKEY_free(pkey);
    // Causes double free
    //    RSA_free(rsa);
    BN_free(bigno);
    X509_free(cert);
    fclose(file_rsa);
    fclose(file_x509);
}
