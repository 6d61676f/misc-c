#include <openssl/bn.h>
#include <openssl/pem.h>
#include <openssl/rsa.h>
#include <stdio.h>
#include <stdlib.h>
int
main(void)
{
    FILE* filePub = fopen("pub.pem", "w+");
    FILE* filePriv = fopen("priv.pem", "w+");
    RSA* rsa = RSA_new();
    BIGNUM* bigNum = BN_new();
    BN_generate_prime_ex(bigNum, 256, 1, NULL, NULL, NULL);
    RSA_generate_key_ex(rsa, 2048, bigNum, 0);
    PEM_write_RSAPublicKey(filePub, rsa);
    PEM_write_RSAPrivateKey(filePriv, rsa, NULL, NULL, 0, NULL, NULL);
    RSA_free(rsa);
    BN_free(bigNum);
    fclose(filePub);
    fclose(filePriv);
    return (0);
}
