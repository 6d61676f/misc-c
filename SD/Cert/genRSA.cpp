#include <cstdint>
#include <iostream>
#include <memory>
#include <openssl/bn.h>
#include <openssl/pem.h>
#include <openssl/rsa.h>

using RSA_ptr = std::unique_ptr<RSA, decltype(&::RSA_free)>;
using BN_ptr = std::unique_ptr<BIGNUM, decltype(&::BN_free)>;
using FILE_ptr = std::unique_ptr<FILE, decltype(&::fclose)>;

int
main(void)
{
    BN_ptr bignum(BN_new(), BN_free);
    BN_generate_prime_ex(bignum.get(), 256, 1, nullptr, nullptr, nullptr);
    RSA_ptr rsa(RSA_new(), RSA_free);
    RSA_generate_key_ex(rsa.get(), 2048, bignum.get(), nullptr);

    FILE_ptr priv(fopen("priv.pem", "wb"), fclose);
    FILE_ptr pub(fopen("pub.pem", "wb"), fclose);

    PEM_write_RSAPrivateKey(priv.get(), rsa.get(), nullptr, nullptr, 0, nullptr,
                            nullptr);
    PEM_write_RSAPublicKey(pub.get(), rsa.get());

    return 1;
}
