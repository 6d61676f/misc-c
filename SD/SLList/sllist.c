#include "sllist.h"
#include "debug.h"
#include <errno.h>
#include <malloc.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

enum sll_attr_vals
{
  SINGLY_LINKED = 1 << 0,
  DOUBLY_LINKED = 1 << 1,
  TYPE_QUEUE = 1 << 2,
  TYPE_STACK = 1 << 3,
};

/** Default list type defined by attributes */
static const sll_attr_t default_attr = SINGLY_LINKED | TYPE_QUEUE;

/** Structure to be used if list is declared as singly linked */
struct sll_element_singly
{
  struct sll_element_singly* next; /**< Pointer to next element*/
  uint8_t* data;                   /**< Pointer to user data */
  uint8_t* key;                    /**< Pointer to key -- allocated by API */
  size_t keyLen;                   /**< length of key used for comparison */
};

/** Structure to be used if list is declared as doubly linked */
struct sll_element_doubly
{
  struct sll_element_doubly* prev; /**< Pointer to previous element */
  struct sll_element_doubly* next; /**< Pointer to next element */
  uint8_t* data;                   /**< Pointer to user data */
  uint8_t* key;                    /**< Pointer to key -- allocated by API */
  size_t keyLen;                   /**< length of key used for comparison */
};

/** List head structure that keeps metadata */
struct sll_head
{
  size_t no;           /**< Number of elements in list -- without sll_head*/
  sll_attr_t sll_attr; /**< Attribute variable describing the list*/
  pthread_mutex_t sll_mutex; /**< Mutex used in sensitive operations */
  void* first;               /**< Pointer to first element */
  void* last;                /**< Pointer to last element */
};

/** Consistency check for sane stuff
 * @param[in] sll_head_p - list head
 * return
 *  -   EXIT_FAILURE
 *  -   EXIT_SUCCESS
 */
static uint_fast8_t
headConsistencyCheck(sll_head_p head, uint8_t getLock)
{
  if (!head) {
    failMessage(EXIT_FAILURE, "%s", "Arg is NULL.");
  }
  if (!head->sll_attr) {
    failMessage(EXIT_FAILURE, "%s", "SLL attribute is zero.");
  }
  if ((head->sll_attr & (SINGLY_LINKED | DOUBLY_LINKED)) ==
      (SINGLY_LINKED | DOUBLY_LINKED)) {
    failMessage(EXIT_FAILURE,
                "%s",
                "List cannot be both SINGLY_LINKED and DOUBLY_LINKED");
  }
  if ((head->sll_attr & (TYPE_STACK | TYPE_QUEUE)) ==
      (TYPE_STACK | TYPE_QUEUE)) {
    failMessage(
      EXIT_FAILURE, "%s", "List cannot be both TYPE_STACK and TYPE_QUEUE");
  }
  if (getLock) {
    if (pthread_mutex_trylock(&head->sll_mutex)) {
      returnMessage(EXIT_FAILURE, "%s", "Could not acquire lock.");
    }
  }
  DEBUG_PRINT("%s", "Consistency check passed");
  return EXIT_SUCCESS;
}

uint_fast8_t
sll_dump_head(const sll_head_p head, uint8_t dumpElems)
{
  if (!head) {
    returnMessage(EXIT_FAILURE, "%s", "Arg is NULL");
  }
  printf("\n-----------SLL HEAD-------------\n");
  printf("no = %zu\n", head->no);
  printf("first = %p\n", head->first);
  printf("last = %p\n", head->last);
  for (int i = 10; i >= 0; i--) {
    printf("| %2d ", i);
  }
  printf("\n");
  for (int i = 10; i >= 0; i--) {
    printf("| %2d ", (head->sll_attr & (1 << i)) ? 1 : 0);
  }
  printf("\n--------------------------------\n");
  void* lastElement = NULL;
  if (dumpElems) {
    if (head->sll_attr & SINGLY_LINKED) {
      size_t count = 0;
      struct sll_element_singly* elem = NULL;
      for (elem = (struct sll_element_singly*)head->first, count = 0;
           count < head->no && (void*)elem != head;
           count++, elem = elem->next) {
        printf(
          "\nElement(%zu) %p -- next %p -- key %p -- keyLen %zu -- data %p\n",
          count + 1,
          elem,
          elem->next,
          elem->key,
          elem->keyLen,
          elem->data);
        for (size_t i = 0; i < elem->keyLen; i++) {
          printf("%#X ", (unsigned)elem->key[i]);
        }
        printf("\n--------------------------------\n");
      }
      lastElement = elem;

    } else if (head->sll_attr & DOUBLY_LINKED) {
      struct sll_element_doubly* elem = NULL;
      for (elem = (struct sll_element_doubly*)head->first, count = 0;
           count < head->no && (void*)elem != head;
           count++, elem = elem->next) {
        printf(
          "\nElement(%zu) %p -- prev %p -- next %p -- key %p -- keyLen %zu "
          "-- data %p\n",
          count + 1,
          elem,
          elem->prev,
          elem->next,
          elem->key,
          elem->keyLen,
          elem->data);
        for (size_t i = 0; i < elem->keyLen; i++) {
          printf("%#X ", (unsigned)elem->key[i]);
        }
        printf("\n--------------------------------\n");
      }
      lastElement = elem;

    } else {
      returnMessage(EXIT_FAILURE, "%s", "Incorrect list type");
    }
    if (count < head->no || (lastElement != head && head->first != NULL)) {
      fprintf(stderr, "List is corrupt....\n");
      return EXIT_FAILURE;
    }
  }

  return EXIT_SUCCESS;
}

uint_fast8_t
sll_attr_singlyLinked(sll_attr_t* sll_attr)
{
  if (!sll_attr) {
    returnMessage(EXIT_FAILURE, "Arg is NULL%c", '\n');
  } else {
    (*sll_attr) |= SINGLY_LINKED;
    (*sll_attr) &= ~DOUBLY_LINKED;
    return EXIT_SUCCESS;
  }
}

uint_fast8_t
sll_attr_doublyLinked(sll_attr_t* sll_attr)
{
  if (!sll_attr) {
    returnMessage(EXIT_FAILURE, "Arg is NULL%c", '\n');
  } else {
    (*sll_attr) |= DOUBLY_LINKED;
    (*sll_attr) &= ~SINGLY_LINKED;
    return EXIT_SUCCESS;
  }
}

uint_fast8_t
sll_attr_stackType(sll_attr_t* sll_attr)
{
  if (!sll_attr) {
    returnMessage(EXIT_FAILURE, "Arg is NULL%c", '\n');
  } else {
    (*sll_attr) |= TYPE_STACK;
    (*sll_attr) &= ~TYPE_QUEUE;
    return EXIT_SUCCESS;
  }
}

uint_fast8_t
sll_attr_queueType(sll_attr_t* sll_attr)
{
  if (!sll_attr) {
    returnMessage(EXIT_FAILURE, "Arg is NULL%c", '\n');
  } else {
    (*sll_attr) |= TYPE_QUEUE;
    (*sll_attr) &= ~TYPE_STACK;
    return EXIT_SUCCESS;
  }
}

sll_head_p
sll_create(const sll_attr_t* sll_attr)
{
  sll_head_p head = NULL;
  head = (sll_head_p)calloc(1, sizeof *head);
  if (!head) {
    returnMessage(head, "%s", "Allocation has failed");
  }
  if (!sll_attr) {
    head->sll_attr = default_attr;
    DEBUG_PRINT("Using default attributes%c", '.');
  } else {
    head->sll_attr = *sll_attr;
    DEBUG_PRINT("Using user-defined attributes%c", '.');
  }
  if (pthread_mutex_init(&head->sll_mutex, NULL)) {
    free(head);
    returnMessage(NULL, "%s", "Mutex initialization has failed.");
  }
  returnMessage(head, "%s", "Allocation has succeded");
}

uint_fast8_t
sll_delete(sll_head_p* headP, sll_elem_fptr cleaner)
{
  if (!headP || headConsistencyCheck(*headP, 1) == EXIT_FAILURE) {
    returnMessage(EXIT_FAILURE, "%s", "Consistency check failed.");
  }
  sll_head_p tempHead = *headP;
  size_t count = 0;
  void* finalElement = NULL;
  if (tempHead->sll_attr & SINGLY_LINKED) {
    struct sll_element_singly *first = NULL, *temp = NULL;
    first = (struct sll_element_singly*)tempHead->first;
    while ((void*)first != tempHead && count < tempHead->no) {
      temp = first->next;
      if (cleaner) {
        cleaner(first->data);
      }
      if (first->key) {
        free(first->key);
      }
      free(first);
      first = temp;
      count++;
    }
    finalElement = first;
    DEBUG_PRINT("%s", "Finished cleaning singly list.");
  } else if (tempHead->sll_attr & DOUBLY_LINKED) {
    struct sll_element_doubly *first = NULL, *temp = NULL;
    first = (struct sll_element_doubly*)tempHead->first;
    while ((void*)first != tempHead && count < tempHead->no) {
      temp = first->next;
      if (cleaner) {
        cleaner(first->data);
      }
      if (first->key) {
        free(first->key);
      }
      free(first);
      first = temp;
      count++;
    }
    finalElement = first;
    DEBUG_PRINT("%s", "Finished cleaning doubly list.");
  }
  if (count < tempHead->no ||
      (finalElement != tempHead && tempHead->first != NULL)) {
    fprintf(stderr, "---List has been corrupted...\n");
  }

  *headP = NULL;

  if (pthread_mutex_unlock(&tempHead->sll_mutex)) {
    fprintf(stderr, "%s\n", "pthread_mutex_unlock has failed");
  }
  if (pthread_mutex_destroy(&tempHead->sll_mutex)) {
    fprintf(stderr, "%s\n", "pthread_mutex_destroy has failed");
  }

  free(tempHead);
  return EXIT_SUCCESS;
}

/** Helper function to populate the list element key
 * @param[in,out] element - list element
 * @param[in] data - user specified data -- used as key if user passed key is
 * NULL
 * @param[in] key - user specified key
 * @param[in] keyLen - length of key used for comparison
 * @param[in] sll_attr - attribute structure used to determine various info
 * @return
 *  -   EXIT_FAILURE
 *  -   EXIT_SUCCESS
 */
static inline uint_fast8_t
setData(uint8_t* element,
        const uint8_t* data,
        const uint8_t* key,
        size_t keyLen,
        sll_attr_t sll_attr)
{
  if (!element) {
    returnMessage(EXIT_FAILURE, "%s", "Element is NULL");
  }
  if (!(data || key) || (key && !keyLen)) {
    returnMessage(
      EXIT_FAILURE, "%s", "At least one of Data or Key must be valid");
  }
  if (sll_attr & SINGLY_LINKED) {
    struct sll_element_singly* sle = (struct sll_element_singly*)element;
    sle->data = (uint8_t*)data;
    if (key && keyLen > 0) {
      DEBUG_PRINT("%s", "Singly Linked -- user-def key");
      sle->key = (uint8_t*)calloc(keyLen, sizeof *sle->key);
      if (!sle->key) {
        fprintf(stderr,
                "Key allocation has failed."
                "Shall user ptr as key.");
        return EXIT_FAILURE;
      } else {
        memmove(sle->key, key, keyLen);
        sle->keyLen = keyLen;
      }
    } else {
      DEBUG_PRINT("%s", "Singly Linked -- data key");
      sle->key = (uint8_t*)calloc(1, sizeof data);
      if (!sle->key) {
        fprintf(stderr, "Key allocation has failed.");
        return EXIT_FAILURE;
      } else {
        memmove(sle->key, &data, sizeof data);
        sle->keyLen = sizeof data;
      }
    }
  } else if (sll_attr & DOUBLY_LINKED) {
    struct sll_element_doubly* sle = (struct sll_element_doubly*)element;
    sle->data = (uint8_t*)data;
    if (key && keyLen > 0) {
      DEBUG_PRINT("%s", "Doubly Linked -- user key");
      sle->key = (uint8_t*)calloc(keyLen, sizeof *sle->key);
      if (!sle->key) {
        fprintf(stderr,
                "Key allocation has failed."
                "Shall user ptr as key.");
        return EXIT_FAILURE;
      } else {
        memmove(sle->key, key, keyLen);
        sle->keyLen = keyLen;
      }
    } else {
      DEBUG_PRINT("%s", "Doubly Linked -- data key");
      sle->key = (uint8_t*)calloc(1, sizeof data);
      if (!sle->key) {
        fprintf(stderr, "Key allocation has failed.");
        return EXIT_FAILURE;
      } else {
        memmove(sle->key, &data, sizeof data);
        sle->keyLen = sizeof data;
      }
    }
  }
  DEBUG_PRINT("Everything went smoothly%c", '.');
  return EXIT_SUCCESS;
}

uint_fast8_t
sll_add_elem(sll_head_p head,
             const uint8_t* data,
             const uint8_t* key,
             size_t keyLen)
{
  if (!(data || key) || (key && keyLen == 0)) {
    returnMessage(
      EXIT_FAILURE, "%s", "At least one of Data or Key must be valid");
  }
  if (headConsistencyCheck(head, 1) == EXIT_FAILURE) {
    returnMessage(EXIT_FAILURE, "%s", "Consistency failed.");
  }
  void* newLastElement = NULL;
  if (head->no == 0) {
    if (head->sll_attr & SINGLY_LINKED) {
      struct sll_element_singly* newElem =
        (struct sll_element_singly*)calloc(1, sizeof *newElem);
      if (!newElem) {
        pthread_mutex_unlock(&head->sll_mutex);
        returnMessage(EXIT_FAILURE, "%s", "Allocation has failed");
      }
      newElem->next = (struct sll_element_singly*)head;
      newLastElement = newElem;
    } else if (head->sll_attr & DOUBLY_LINKED) {
      struct sll_element_doubly* newElem =
        (struct sll_element_doubly*)calloc(1, sizeof *newElem);
      if (!newElem) {
        pthread_mutex_unlock(&head->sll_mutex);
        returnMessage(EXIT_FAILURE, "%s", "Allocation has failed");
      }
      newElem->next = (struct sll_element_doubly*)head;
      newElem->prev = (struct sll_element_doubly*)head;
      newLastElement = newElem;
    }
    head->first = newLastElement;
    head->last = newLastElement;
    DEBUG_PRINT("Added first element to list%c", '.');
  } else {
    if (head->sll_attr & SINGLY_LINKED) {
      struct sll_element_singly* last = (struct sll_element_singly*)head->last;
      last->next = (struct sll_element_singly*)calloc(
        1, sizeof(struct sll_element_singly));
      if (!last->next) {
        pthread_mutex_unlock(&head->sll_mutex);
        returnMessage(EXIT_FAILURE, "%s", "Allocation has failed");
      }
      last->next->next = (struct sll_element_singly*)head;
      newLastElement = last->next;
    } else if (head->sll_attr & DOUBLY_LINKED) {
      struct sll_element_doubly* last = (struct sll_element_doubly*)head->last;
      last->next = (struct sll_element_doubly*)calloc(
        1, sizeof(struct sll_element_doubly));
      if (!last->next) {
        pthread_mutex_unlock(&head->sll_mutex);
        returnMessage(EXIT_FAILURE, "%s", "Allocation has failed");
      }
      last->next->next = (struct sll_element_doubly*)head;
      last->next->prev = last;
      newLastElement = last->next;
    }
    head->last = newLastElement;
    DEBUG_PRINT("Added to the end of to list%c", '.');
  }
  head->no++;
  setData((uint8_t*)newLastElement, data, key, keyLen, head->sll_attr);
  DEBUG_PRINT("We currently have %zu elements.", head->no);

  pthread_mutex_unlock(&head->sll_mutex);
  return EXIT_SUCCESS;
}

uint_fast8_t
sll_for_each(sll_head_p head, sll_elem_fptr funky)
{
  size_t count = 0;
  void* lastElement = NULL;
  if (!funky || headConsistencyCheck(head, 1) == EXIT_FAILURE) {
    returnMessage(EXIT_FAILURE, "%s", "Consistency check failed.");
  }
  if (head->sll_attr & SINGLY_LINKED) {
    struct sll_element_singly* element;
    for (count = 0, element = (struct sll_element_singly*)head->first;
         count < head->no && (void*)element != head;
         count++, element = element->next) {
      if (element->data) {
        DEBUG_PRINT("About to run function on element %zu", count);
        funky(element->data);
      } else {
        DEBUG_PRINT("Data is NULL for element %zu", count);
      }
    }
    lastElement = element;
  } else if (head->sll_attr & DOUBLY_LINKED) {
    struct sll_element_doubly* element;
    for (count = 0, element = (struct sll_element_doubly*)head->first;
         count < head->no && (void*)element != head;
         count++, element = element->next) {
      if (element->data) {
        DEBUG_PRINT("About to run function on element %zu", count);
        funky(element->data);
      } else {
        DEBUG_PRINT("Data is NULL for element %zu", count);
      }
    }
    lastElement = element;
  }
  if (count < head->no || (lastElement != head && head->first != NULL)) {
    printf("\n-------List is fucked up..count = %zu lastElement = %p\n",
           count,
           lastElement);
  }
  DEBUG_PRINT("%s", "Finished for_each");
  pthread_mutex_unlock(&head->sll_mutex);
  return EXIT_SUCCESS;
}

uint_fast8_t
sll_pop_apply(sll_head_p head, sll_elem_fptr funky)
{
  void* data = sll_pop_get(head);
  if (funky) {
    funky((uint8_t*)data);
    DEBUG_PRINT("%s", "Ran callback on popped element.");
    return EXIT_SUCCESS;
  } else {
    returnMessage(EXIT_FAILURE, "%s", "Arg is NULL.");
  }
}

void*
sll_pop_get(sll_head_p head)
{
  void* removedElement = NULL;
  void* removedData = NULL;
  uint_fast8_t listType = SINGLY_LINKED;

  if (headConsistencyCheck(head, 1) == EXIT_FAILURE) {
    returnMessage(NULL, "%s", "Consistency check failed.");
  }

  if (head->no == 0) {
    pthread_mutex_unlock(&head->sll_mutex);
    returnMessage(NULL, "%s", "List is empty...Add some elements first");
  } else if (head->no == 1) {
    removedElement = head->first;
    if (head->sll_attr & SINGLY_LINKED) {
      removedData = ((struct sll_element_singly*)head->first)->data;
      listType = SINGLY_LINKED;
    } else if (head->sll_attr & DOUBLY_LINKED) {
      removedData = ((struct sll_element_doubly*)head->first)->data;
      listType = DOUBLY_LINKED;
    }
    head->first = head->last = NULL;
    head->no = 0;
    DEBUG_PRINT("%s", "Popped the last element.");
  } else {
    if (head->sll_attr & TYPE_QUEUE) {
      if (head->sll_attr & SINGLY_LINKED) {
        DEBUG_PRINT("%s", "POPPING TYPE_QUEUE SINGLY_LINKED");
        listType = SINGLY_LINKED;
        struct sll_element_singly* first =
          (struct sll_element_singly*)head->first;
        head->first = first->next;
        removedElement = first;
        removedData = first->data;
      } else if (head->sll_attr & DOUBLY_LINKED) {
        DEBUG_PRINT("%s", "POPPING TYPE_QUEUE DOUBLY_LINKED");
        listType = DOUBLY_LINKED;
        struct sll_element_doubly* first =
          (struct sll_element_doubly*)head->first;
        head->first = first->next;
        if ((void*)first->next != head &&
            first->next) { /*XXX Can we reach this? */
          first->next->prev = (struct sll_element_doubly*)head;
        }
        removedElement = first;
        removedData = first->data;
      }
      head->no--;
    } else if (head->sll_attr & TYPE_STACK) {
      if (head->sll_attr & SINGLY_LINKED) {
        DEBUG_PRINT("%s", "POPPING TYPE_STACK SINGLY_LINKED");
        listType = SINGLY_LINKED;
        size_t count;
        struct sll_element_singly* elem_before_last =
          (struct sll_element_singly*)head->first;
        struct sll_element_singly* elem_last =
          ((struct sll_element_singly*)head->first)->next;
        for (count = 0; count < head->no && elem_last != NULL; count++) {
          if ((void*)elem_last->next == head) {
            elem_before_last->next = (struct sll_element_singly*)head;
            head->last = elem_before_last;
            removedData = elem_last->data;
            removedElement = elem_last;
            elem_last = NULL;
          } else {
            elem_before_last = elem_last;
            elem_last = elem_last->next;
          }
        }
      } else if (head->sll_attr & DOUBLY_LINKED) {
        DEBUG_PRINT("%s", "POPPING TYPE_STACK DOUBLY_LINKED");
        listType = DOUBLY_LINKED;
        struct sll_element_doubly* elem_last =
          (struct sll_element_doubly*)head->last;
        elem_last->prev->next = (struct sll_element_doubly*)head;
        head->last = elem_last->prev;
        removedData = elem_last->data;
        removedElement = elem_last;
      }
      head->no--;
    }
  }
  DEBUG_PRINT("%s", "Finished pop");
  if (!(removedElement && removedData)) {
    pthread_mutex_unlock(&head->sll_mutex);
    failMessage(
      NULL, "%s", "In this scenario the removed element should not be NULL");
  } else {
    if (listType == SINGLY_LINKED) {
      if (((struct sll_element_singly*)removedElement)->key != NULL) {
        free(((struct sll_element_singly*)removedElement)->key);
      }
    } else if (listType == DOUBLY_LINKED) {
      if (((struct sll_element_doubly*)removedElement)->key != NULL) {
        free(((struct sll_element_doubly*)removedElement)->key);
      }
    }
    free(removedElement);
  }

  pthread_mutex_unlock(&head->sll_mutex);
  return removedData;
}
