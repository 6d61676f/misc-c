#include "sllist.h"
#include <stdint.h>
#include <stdio.h>

#ifndef FUBAR
#define FUBAR 1 << 12
#endif

void
printShit(uint8_t* data)
{
  printf("%zu\n", *(size_t*)data);
}

void
printFreeShit(uint8_t* data)
{
  printf("%zu\n", *(size_t*)data);
  free(data);
}

int
main(void)
{
  sll_head_p sllsingle = NULL;
  sll_head_p slldouble = NULL;
  sll_attr_t slattr = 0;
  size_t* val = NULL;

  sll_attr_queueType(&slattr);
  sll_attr_singlyLinked(&slattr);
  sllsingle = sll_create(NULL);

  sll_attr_doublyLinked(&slattr);
  sll_attr_stackType(&slattr);
  slldouble = sll_create(&slattr);

  for (size_t i = 1; i <= FUBAR; i++) {
    val = (size_t*)calloc(1, sizeof *val);
    *val = i;
    sll_add_elem(sllsingle, (uint8_t*)val, (uint8_t*)val, sizeof *val);
    val = (size_t*)calloc(1, sizeof *val);
    *val = i;
    sll_add_elem(slldouble, (uint8_t*)val, (uint8_t*)val, sizeof *val);
  }

  printf("Finished allocation...Waiting\n");
  getchar();

#ifdef POP_CLEAN
  while (sll_pop_apply(sllsingle, printFreeShit) != EXIT_FAILURE) {
    sll_dump_head(sllsingle, 0);
  }
  while (sll_pop_apply(slldouble, printFreeShit) != EXIT_FAILURE) {
    sll_dump_head(slldouble, 0);
  }
  free(sllsingle);
  free(slldouble);
  sllsingle = NULL;
  slldouble = NULL;
#else
  sll_for_each(sllsingle, printShit);
  sll_dump_head(sllsingle, 0);
  sll_delete(&sllsingle, (sll_elem_fptr)free);

  sll_for_each(slldouble, printShit);
  sll_dump_head(slldouble, 0);
  sll_delete(&slldouble, (sll_elem_fptr)free);
#endif

  return EXIT_SUCCESS;
}
