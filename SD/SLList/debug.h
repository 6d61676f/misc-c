#ifndef DEBUG_LIB_H
#define DEBUG_LIB_H

#ifndef RIDE_THE_LIGHTNING
#define RIDE_THE_LIGHTNING                                                     \
  do {                                                                         \
    fprintf(                                                                   \
      stderr, "\nDumping core at %s:%s:%d\n", __FILE__, __func__, __LINE__);   \
    (*((int*)(0x00)) = 0x0FFF);                                                \
  } while (0)
#endif // CORE Special

#ifdef VERBOSE_MESSAGE

#define failMessage(exCode, fmt, ...)                                          \
  do {                                                                         \
    fprintf(stderr,                                                            \
            "%s:%d:%s\n\t\'" fmt "\'\n",                                       \
            __FILE__,                                                          \
            __LINE__,                                                          \
            __func__,                                                          \
            __VA_ARGS__);                                                      \
    RIDE_THE_LIGHTNING;                                                        \
  } while (0)

#define returnMessage(retCode, fmt, ...)                                       \
  do {                                                                         \
    fprintf(stderr,                                                            \
            "%s:%d:%s\n\t\'" fmt "\'\n",                                       \
            __FILE__,                                                          \
            __LINE__,                                                          \
            __func__,                                                          \
            __VA_ARGS__);                                                      \
    return retCode;                                                            \
  } while (0)

#else

#define failMessage(exCode, fmt, ...)                                          \
  do {                                                                         \
    fprintf(stderr,                                                            \
            "%s:%d:%s\n\t\'" fmt "\'\n",                                       \
            __FILE__,                                                          \
            __LINE__,                                                          \
            __func__,                                                          \
            __VA_ARGS__);                                                      \
    return exCode;                                                             \
  } while (0)

#define returnMessage(retCode, fmt, ...)                                       \
  do {                                                                         \
    return retCode;                                                            \
  } while (0)

#endif // VERBOSE_MESSAGE FLAG

#ifdef DEBUG_MESSAGE

#define DEBUG_PRINT(fmt, ...)                                                  \
  do {                                                                         \
    fprintf(stderr,                                                            \
            "%s:%d:%s\n\t\'" fmt "\'\n",                                       \
            __FILE__,                                                          \
            __LINE__,                                                          \
            __func__,                                                          \
            __VA_ARGS__);                                                      \
  } while (0)
#else

#define DEBUG_PRINT(fmt, ...)                                                  \
  do {                                                                         \
  } while (0)

#endif // DEBUG_MESSAGE FLAG

#endif // DEBUG_LIB_H
