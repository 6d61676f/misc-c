#ifndef SLLIST_H
#define SLLIST_H
#include <inttypes.h>
#include <stdlib.h>

/** generic function pointer given by the user to manipulate the data */
typedef void (*sll_elem_fptr)(uint8_t* data);
/** Used as a bitmask to specify the list creation options*/
typedef uint_fast64_t sll_attr_t;
/** The head element of the list that holds various info about the list*/
struct sll_head;
/** Actual handler given to the user*/
typedef struct sll_head* sll_head_p;

/** Init function that takes a pointer to an attribute list 'sll_attr_t' and
 * returns 'sll_head_p'
 * @param[in] sll_attr - pointer to attribute list that modifies the behaviour
 * of the list
 * @return
 *          - pointer to struct sll_head
 *          - NULL
 */
sll_head_p
sll_create(const sll_attr_t* sll_attr);

/** Delete function that deallocates the list elements and head
 * @param[in, out] headP - pointer to list head pointer containing the list
 * @param[in] cleaner - cleaner function called on every data element in the
 * list
 * @return
 *      - EXIT_FAILURE
 *      - EXIT_SUCCESS
 */
uint_fast8_t
sll_delete(sll_head_p* headP, sll_elem_fptr cleaner);

/** Sets the list attribute list with the option for singly linked
 * @param[in,out] sll_attr - attribute variable
 * @return
 *  -   EXIT_FAILURE
 *  -   EXIT_SUCCESS
 */
uint_fast8_t
sll_attr_singlyLinked(sll_attr_t* sll_attr);

/** Sets the list attribute list with the option for doubly linked
 * @param[in,out] sll_attr - attribute variable
 * @return
 *  -   EXIT_FAILURE
 *  -   EXIT_SUCCESS
 */
uint_fast8_t
sll_attr_doublyLinked(sll_attr_t* sll_attr);

/** Sets the list attribute list with the option for stack type
 * @param[in,out] sll_attr - attribute variable
 * @return
 *  -   EXIT_FAILURE
 *  -   EXIT_SUCCESS
 */
uint_fast8_t
sll_attr_stackType(sll_attr_t* sll_attr);

/** Sets the list attribute list with the option for queue type
 * @param[in,out] sll_attr - attribute variable
 * @return
 *  -   EXIT_FAILURE
 *  -   EXIT_SUCCESS
 */
uint_fast8_t
sll_attr_queueType(sll_attr_t* sll_attr);

/** Helper funtion that calls another function on every data element in the list
 * @param[in,out] head - list head
 * @param[in,out] funky - function to be called on every argument
 * @return
 *  -   EXIT_FAILURE - if head is NULL
 *  -   EXIT_SUCCESS
 */
uint_fast8_t
sll_for_each(sll_head_p head, sll_elem_fptr funky);

/** The single most important function..\n
 * Add a new element that points to data and can be found by key\n
 * If key is NULL or cannot be allocated the data pointer is used instead as
 * key\n
 * @param[in,out] head - list head
 * @param[in] data - pointer to user allocated data to be kept in list
 * @param[in] key - key that identifies an element in the list
 * @param[in] keyLen - length of key
 * @return
 *  -   EXIT_SUCCESS
 *  -   EXIT_FAILURE
 */
uint_fast8_t
sll_add_elem(sll_head_p head,
             const uint8_t* data,
             const uint8_t* key,
             size_t keyLen);

/** Helper function that dumps the head metadata and every element's key and
 * keyLen
 * @param[in] head - list head
 * @param[in] dumpElems - boolean flag to trigger the dump of elements
 * @return
 *  -   EXIT_FAILURE
 *  -   EXIT_SUCCESS
 */
uint_fast8_t
sll_dump_head(const sll_head_p head, uint8_t dumpElems);

/** Pop functions that removes one element from the list depending on the type\n
 * If the list is TYPE_QUEUE we remove the first element\n
 * If the list is TYPE_STACK we remove the last element\n
 * We apply the function on the data held in the element\n
 * After funky is called the list element is removed and the user should free
 * the data.
 * @param[in,out] head - list head
 * @param[in,out] funky - gets called on every data element in the list
 * @return
 *  -   EXIT_FAILURE
 *  -   EXIT_SUCCESS
 */
uint_fast8_t
sll_pop_apply(sll_head_p head, sll_elem_fptr funky);

/** Pop functions that removes one element from the list depending on the type\n
 * If the list is TYPE_QUEUE we remove the first element\n
 * If the list is TYPE_STACK we remove the last element\n
 * The list element is removed and the user should free the data.
 * @param[in,out] head - list head
 * @return
 *  -   NULL
 *  -   user data kept in the element
 */
void*
sll_pop_get(sll_head_p head);

#endif
