#include "coada.h"
#include <malloc.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
nod_t*
createNod(uint32_t id)
{
  nod_t* nou = calloc(1, sizeof *nou);
  nou->id = id;
  return (nou);
}
nod_t*
addNod(nod_t* element, nod_t* start)
{
  nod_t* i = 0;
  for (i = start; i->next != 0; i = i->next)
    ;
  i->next = element;
  return (i->next);
}

nod_t*
stergeNod(uint32_t id, nod_t* start)
{

  nod_t* gasit = 0;

  /*Daca e primul nod si singurul*/
  if (start->id == id && start->next == 0) {
    free(start);
    return (0);
    /* Daca e primul si nu e singurul*/
  } else if (start->id == id && start->next != 0) {
    nod_t* noulStart = start->next;
    free(start);
    return (noulStart);
  }

  for (gasit = start; gasit->next != 0 && gasit->next->id != id;
       gasit = gasit->next)
    ;

  /*Daca nu este ultimul element si este cel cautat*/
  if (gasit->next != 0 && gasit->next->id == id) {
    nod_t* ultimul = gasit->next->next;
    free(gasit->next);
    gasit->next = ultimul;

    /*Daca este ultimul element si este cel cautat*/
  } else if (gasit->next == 0 && gasit->id == id) {
    free(gasit);
  }

  return (start);
}

void
printCoada(nod_t* start)
{
  for (; start != 0; start = start->next) {
    printf("%u %s", start->id, (start->next != 0) ? ("-> ") : ("\n"));
  }
}
nod_t*
findNod(uint32_t id, nod_t* start)
{
  nod_t* gasit = 0;
  for (gasit = start; gasit != 0 && gasit->id != id; gasit = gasit->next)
    ;
  return (gasit);
}
void
stergeCoada(nod_t* start)
{
  nod_t* curent = start;

  while (curent != 0) {
    nod_t* urmator = curent->next;
    free(curent);
    curent = urmator;
  }
}
