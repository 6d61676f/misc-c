#ifndef COADA_H
#define COADA_H
#include <stdint.h>

typedef struct element nod_t;
typedef struct element nod_next_t;

struct element
{
  uint32_t id;
  nod_next_t* next;
};

nod_t* createNod(uint32_t id);
nod_t* addNod(nod_t* element, nod_t* start);
nod_t* stergeNod(uint32_t id, nod_t* start);
nod_t* findNod(uint32_t id, nod_t* start);

void printCoada(nod_t* start);
void stergeCoada(nod_t* start);

#endif
