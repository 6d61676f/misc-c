#include "coada.h"
#include <stdio.h>
#include <stdlib.h>
int
main(void)
{
  nod_t* start = createNod(1);
  nod_t* a = createNod(2);
  nod_t* b = createNod(3);
  nod_t* c = createNod(4);
  nod_t* ultim = addNod(a, start);
  printf("%s %u\n", "Ultimul nod este:", ultim->id);
  ultim = addNod(b, start);
  printf("%s %u\n", "Ultimul nod este:", ultim->id);
  ultim = addNod(c, start);
  start = stergeNod(1, start);
  printf("%s %u\n", "Primul nod este:", start->id);
  printf("%s %u\n", "Ultimul nod este:", ultim->id);
  printCoada(start);
  stergeCoada(start);

  return (EXIT_SUCCESS);
}
