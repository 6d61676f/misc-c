#ifndef CLIST_H
#define CLIST_H
#include <stdint.h>
#include <stdlib.h>
typedef struct listNode listNode_t;
typedef struct listNode listHead_t;
struct listNode
{
  struct listNode* next;
  void* data;
  char dataType[10];
  size_t dataSize;
};

listNode_t* createNode(void* dataAddress, char dataType[10], size_t dataSize);
listHead_t* initHead(void* dataAddress, char dataType[10], size_t dataSize);
uint8_t addNode(listNode_t* dataNou, listHead_t* listHead);
void printList(listHead_t* head);
uint8_t equalsNode(listNode_t* listNode, void* data, size_t dataSize);
listHead_t* deleteNode(listHead_t* head, void* data, size_t dataSize);
void freeNode(listHead_t* node);
void deleteList(listHead_t** head);
#endif // CLIST_H
