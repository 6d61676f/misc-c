#include <clist.h>
#include <stdio.h>
#include <stdlib.h>
int
main(void)
{
  char adi[] = "Adi";
  char laur[] = "Laur";
  char radu[] = "Radu";

  listHead_t* lHead = initHead(adi, "CHAR", sizeof adi);
  addNode(createNode(laur, "CHAR", sizeof laur), lHead);
  addNode(createNode(radu, "CHAR", sizeof radu), lHead);

  FILE* f = fopen("/etc/fstab", "rb");
  if (f) {
    char c = 0x0;
    char* buffer = 0;
    size_t count = 0;
    while (fread(&c, 1, 1, f) == 1) {
      count++;
    }
    buffer = malloc(count);
    fseek(f, 0, SEEK_SET);
    fread(buffer, count, 1, f);
    addNode(createNode(buffer, "BFILE", count), lHead);
    free(buffer);
    fclose(f);
  }

  /*
  lHead = deleteNode(lHead, laur, sizeof laur);
  lHead = deleteNode(lHead, radu, sizeof radu);
  lHead = deleteNode(lHead, adi, sizeof adi);
  */

  printList(lHead);
  deleteList(&lHead);
  return (EXIT_SUCCESS);
}
