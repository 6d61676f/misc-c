#include <clist.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

listNode_t*
createNode(void* dataAddress, char dataType[10], size_t dataSize)
{
  listNode_t* nou = malloc(sizeof(*nou));
  if (nou) {
    nou->next = 0;
    nou->dataSize = dataSize;
    strncpy(nou->dataType, dataType, 10);
    if (dataAddress != 0) {
      nou->data = malloc(dataSize);
      if (nou->data != 0) {
        memcpy(nou->data, dataAddress, dataSize);
      } else {
        free(nou)
        return (0);
      }
    }
    return nou;
  } else {
    free(nou);
    return (0);
  }
}

listHead_t*
initHead(void* dataAddress, char dataType[10], size_t dataSize)
{
  listHead_t* head = createNode(dataAddress, dataType, dataSize);
  if (head) {
    head->next = head;
    return (head);
  } else {
    printf("Head not initialized\n");
    return (0);
  }
}

uint8_t
addNode(listNode_t* dataNou, listHead_t* listHead)
{
  if (listHead == 0) {
    printf("ListHead is NULL\n");
    return (0);
  } else {
    if (dataNou == 0) {
      printf("New Node is NULL\n");
      return (0);
    } else {
      dataNou->next = listHead->next;
      listHead->next = dataNou;
      return (1);
    }
  }
}

void
printList(listHead_t* head)
{
  if (head) {
    void* adresa = head;
    do {
      unsigned i = 0;
      uint8_t* byte = head->data;
      printf("|");
      for (i = 0; i < head->dataSize; i++) {
        printf("0x%x%c", byte[i], i != head->dataSize - 1 ? ' ' : '|');
      }
      printf(" %s %lu -> ", head->dataType, (unsigned long)head->dataSize);
      head = head->next;
    } while (head != adresa);
  }
  printf("\n");
}

uint8_t
equalsNode(listNode_t* listNode, void* data, size_t dataSize)
{
  if (listNode != 0) {
    if (dataSize == listNode->dataSize) {
      if (memcmp(listNode->data, data, dataSize) == 0)
        return (1);
    }
  }
  return (0);
}

listHead_t*
deleteNode(listHead_t* head, void* data, size_t dataSize)
{
  if (head) {
    void* start = head;
    listNode_t* current = head->next;
    do {
      if (equalsNode(current, data, dataSize)) {
        head->next = current->next;
        freeNode(current);
        if (current == start) {
          start = head;
        }
        if (current == head) {
          return (0);
        }
      }
      head = head->next;
      current = head->next;
    } while (head != start);
  }
  return (head);
}

void
deleteList(listHead_t** head)
{
  if (head != 0 && *head != 0) {
    listHead_t* newHead = *head;
    if (newHead == newHead->next) {
      freeNode(newHead);
    } else {
      void* headStart = *head;
      listNode_t *prev = *head, *current = (*head)->next;
      do {
        freeNode(prev);
        prev = current;
        current = current->next;
      } while (current != headStart);
      freeNode(prev);
    }
    *head = 0;
  }
}

void
freeNode(listHead_t* node)
{
  if (node) {
    if (node->data) {
      free(node->data);
      node->data = 0;
    }
    node->dataSize = 0;
    node->next = 0;
    free(node);
  }
}
