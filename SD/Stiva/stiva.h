#ifndef STIVA_H
#define STIVA_H
#include <stdint.h>

typedef struct element nod_t;
typedef struct element nod_next_t;

struct element
{
  uint32_t id;
  nod_next_t* next;
};

nod_t* createNod(uint32_t id);
nod_t* push(nod_t* start, nod_t* nou);
nod_t* pop(uint32_t* id, nod_t* start);

void printStiva(nod_t* head);
void stergeStiva(nod_t* start);

#endif
