#include "stiva.h"
#include <malloc.h>
#include <stdint.h>

nod_t*
createNod(uint32_t id)
{
  nod_t* nou = calloc(1, sizeof *nou);
  nou->id = id;
  return (nou);
}

nod_t*
push(nod_t* start, nod_t* nou)
{
  nou->next = start;
  return (nou);
}

nod_t*
pop(uint32_t* id, nod_t* start)
{
  nod_t* next = 0;
  if (start == 0) {
    return 0;
  }
  next = start->next;
  if (id != 0) {
    *id = start->id;
  }
  free(start);
  return (next);
}
void
printStiva(nod_t* head)
{
  for (; head != 0; head = head->next) {
    printf("%u %s", head->id, (head->next != 0) ? ("-> ") : ("\n"));
  }
}

void
stergeStiva(nod_t* start)
{
  nod_t* curent = start;

  while (curent != 0) {
    nod_t* urmator = curent->next;
    free(curent);
    curent = urmator;
  }
}
