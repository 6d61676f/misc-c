#include "stiva.h"
#include <stdlib.h>
int
main(void)
{
  nod_t* start = createNod(1);
  nod_t* a = createNod(2);
  nod_t* b = createNod(3);
  nod_t* c = createNod(4);
  start = push(start, a);
  start = push(start, b);
  start = push(start, c);
  printStiva(start);
  start = pop(0, start);
  start = pop(0, start);
  printStiva(start);
  stergeStiva(start);

  return (EXIT_SUCCESS);
}
