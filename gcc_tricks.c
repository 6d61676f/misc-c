/* Cover each example from the article */
/* https://medium.com/@renaudcerrato/the-best-gcc-tips-and-tricks-you-must-know-33e788020221
 */
/* The site format is rather annoying and i preferred to extract the info here
 */
/* Also check out
 * https://gcc.gnu.org/onlinedocs/gcc/Instrumentation-Options.html */
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/* Macro Magic: Stringization -- if that's a word, Concatenation */
/* The first one simply replaces the argument with its actual name */
/* The second one concatenates two arguments while exapanding them */
#define PASSERT(EXP)                                                           \
  do {                                                                         \
    if (!(EXP)) {                                                              \
      fprintf(stderr,                                                          \
              "%s:%d - Assertion (%s) has failed.\n",                          \
              __FILE__,                                                        \
              __LINE__,                                                        \
              #EXP);                                                           \
    }                                                                          \
  } while (0)

struct some_smart_callback
{
  const char* name;
  void (*function)(void);
};

#define DEFAULT_CALLBACK(name)                                                 \
  {                                                                            \
#name, name##_func                                                         \
  }

#define STUB_FUNCTION(name)                                                    \
  void name##_func(void) { printf("Stub command for %s\n", #name); }

void
cleanup_func(int* val)
{
  printf(
    "Running cleanup for int variable(%p) of value %d\n", (void*)val, *val);
}

/* Instrument functions */
/* These two functions get called for entry/exit into functions */
/* Must be compiled -finstrument-functions and attributes used */
/* to not recurse infinitely. */
/* Symbols can be deduced using nm or gdb */
__attribute__((no_instrument_function)) void
__cyg_profile_func_enter(void* this_fn, void* call_site)
{
  printf("\nEntering %p called at %p\n", this_fn, call_site);
}
__attribute__((no_instrument_function)) void
__cyg_profile_func_exit(void* this_fn, void* call_site)
{
  printf("Exiting %p called at %p\n", this_fn, call_site);
}

/* This one is tricky and interesting. We can define linker time */
/* wrapper functions to modify how existing symbols work. */
/* The option passed to the linker is `--wrap=symbol`. */
/* -Wl,--wrap,symbol */
/* The wrapper function must be named `__wrap_symbol` and we */
/* must define the real symbol using `__real_symbol`. */
/*  Let's assume `void * malloc(size_t size)`. */

/*****************************************************************/
/* Note: It seems that NOT defining this function as weak	    */
/* forces us to either: 1) comment out __wrap_malloc because	    */
/* it calls __real_malloc and the symbol cannot be resolved.	    */
/* 2) define it with weak, BUT the linker options are not needed */
/* anymore and the wrapping happens automatically. MAGIC XXX	    */
/*****************************************************************/
#if WRAP_CU_DE_TOATE
extern void* /*__attribute__((weak))*/
__real_malloc(size_t size);

void*
__wrap_malloc(size_t size)
{
  printf("Someone tried to allocate %zu bytes\n", size);
  return __real_malloc(size);
}
#endif

struct some_struct
{
  int some_int;
  char some_char;
};

union some_union
{
  int some_int;
  char some_char;
};

/* These attributes mark functions to be run before and after `main` */
__attribute__((constructor)) void
before_main()
{
  printf("\n\nBefore main\n\n");
}
__attribute__((destructor)) void
after_main()
{
  printf("\n\nAfter main\n\n");
}

/* This attribute causes a function symbol to be emmited as weak */
/* instead of global. We can thus override it in our on compilation unit. */
/* A weak symbol that is not found at link time evaluates to NULL. */
extern void
some_optional_function(void) __attribute__((weak));

int
main(void)
{

  /* This attribute runs a given function when a variable */
  /*   goes out of scope. */
  int some_int __attribute__((cleanup(cleanup_func)));
  some_int = 10;

  /* If we don't link a library containing this symbol or declare it */
  /*   on our own this call shall cause a segmentation fault. */
  some_optional_function();

  /* Let's say that we somehow don't know that `some_int` may be an int */
  /*   and we want to declare another variable with the same type. */
  __auto_type some_other_var = some_int;

  free(malloc(some_other_var));

  /* Designated Initializers */
  int arr[10] = { [0] = 10, [8] = 1 };
  int range_arr[] = { [0 ... 5] = 1,
                      [6 ... 10] = 2 }; /* range-initialization */
  struct some_struct ss1 = { .some_int = 10, .some_char = 'a' };
  struct some_struct ss_arr[] = {
    [1].some_char = 'c',
    [1].some_int = ss1.some_int,
    [0].some_char = ss1.some_char,
    [0].some_int = 55,
  };

  for (size_t i = 0; i < sizeof(ss_arr) / sizeof(ss_arr[0]); ++i) {
    printf(
      "some_int %d, some_char %c\n", ss_arr[i].some_int, ss_arr[i].some_char);
  }

  for (size_t i = 0; i < sizeof(arr) / sizeof(arr[0]); ++i) {
    printf("%d ", arr[i]);
  }
  printf("\n");

  for (size_t i = 0; i < sizeof(range_arr) / sizeof(range_arr[0]); ++i) {
    printf("%d ", range_arr[i]);
  }
  printf("\n");

  /* Cast to union... pretty self explanatory */
  union some_union uni = (union some_union)0x23;
  printf("%c %d\n", uni.some_char, uni.some_int);

  /* Unnamed structure fields + Designated Initializer */
  /* Also alignment requirements */
  /* NOTE: Packed applies to the structure and EVERY member inside */
  /* aligned applies ONLY to the structure/type for which it's specified */
  struct __attribute__((packed))
  {
    int some_int;
    char __attribute__((aligned(8))) some_char;
    union
    {
      int u_some_int;
      char u_some_char;
    } u;
  } unnamed_struct = { .some_int = 5, .some_char = 'a', .u.u_some_int = 10 };

  printf("\nunnamed(%zu) vals %d %c %d\n",
         sizeof(unnamed_struct),
         unnamed_struct.some_int,
         unnamed_struct.some_char,
         unnamed_struct.u.u_some_int);

  /* Compund literals */
  /* i.e. Anonymous lvalues */
  /* Also we show nested functions, i.e. functions declared */
  /* inside a block of code */
  {
    void print_strings(char** vals)
    {
      for (; *vals; ++vals) {
        printf("%s ", *vals);
      }
      printf("\n");
    }
    print_strings((char*[]){ "Dude", "This", "Is", "Nais", NULL });
  }

  /* Flexible array members */
  /* Unnamed structs */
  /* Note that elems[] does not take any space */
  {
    struct
    {
      size_t len;
      int elems[];
    } * flex_arr;

    struct
    {
      size_t len;
      int elems[0];
    } * zero_arr;

    printf("Sizeof struct with zero array %zu\n", sizeof(*zero_arr));
    printf("Sizeof struct with flexible array %zu\n", sizeof(*flex_arr));

    flex_arr = calloc(1, sizeof(*flex_arr) + 3 * sizeof(int));
    flex_arr->len = 3;
    for (size_t i = 0; i < flex_arr->len; ++i) {
      flex_arr->elems[i] = i + 1;
    }
    free(flex_arr);
  }

  /* Variable length arrays */
  /* We can assign the size for a static array at runtime */
  srand(time(NULL));
  int arr_size = rand() % 17;
  char vla_char[arr_size];
  printf("sizeof(vla_char) = %zu\n", sizeof vla_char);

  /* Print the offset of a member in a structure */
  struct three_type_struct
  {
    int first_int;
    char second_char;
    long third_long;
  };

  printf("first_int(%zu) second_char(%zu) third_long(%zu)\n",
         offsetof(struct three_type_struct, first_int),
         offsetof(struct three_type_struct, second_char),
         offsetof(struct three_type_struct, third_long));

  {
    /* This one is a bit confusing. You basically instruct the compiler */
    /* that any function taking as a paramter a union with this attribute */
    /* can be passed directly any of the types containted within the union. */
    union __attribute__((transparent_union)) tunion
    {
      int some_int;
      unsigned int some_unsigned;
      unsigned char some_char[sizeof(int)];
    };

    void print_tu(union tunion uni)
    {
      printf("%d %u \n", uni.some_int, uni.some_unsigned);
      for (size_t i = 0; i < sizeof(int); i++) {
        printf("%02X ", (unsigned)uni.some_char[i]);
      }
      printf("\n");
    }
    print_tu(10);
  }

  PASSERT(0 == 1);
  {
    STUB_FUNCTION(quit)
    struct some_smart_callback quit = DEFAULT_CALLBACK(quit);
    quit.function();
  }
}

void
some_optional_function(void)
{
  printf("\nWe didn't cause a SEGFAULT because I was declared in here."
         "Try commenting me out...\n");
}

/* Here we show case ranges. Basically matching intervals */
/* in a case. */
int
figure_to_int(char c)
{
  switch (c) {
    case '0' ... '9':
      return c - '0';
    default:
      printf("Call me with a some_char representing a numeric figure\n");
      return -1;
  }
}
