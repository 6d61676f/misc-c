#include <fcntl.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#define MEMORY "/myMemory"
#define NAMEDSEM "/mySem"
#define ARRSIZE 0x1000
#define MEMSIZE sizeof(int) * ARRSIZE
int
main(void)
{
  sem_t* memsem = NULL;
  int* memory = NULL;
  int whodafuqami = 0;
  int fd = -1;
  int fdsem = -1;
  int i = 0;

  fd = shm_open(MEMORY, O_CREAT | O_RDWR | O_TRUNC, 0777);
  fdsem = shm_open(NAMEDSEM, O_CREAT | O_RDWR | O_TRUNC, 0777);
  if (fd < 0 || fdsem < 0) {
    perror("shm_open");
    return (EXIT_FAILURE);
  }
  if (ftruncate(fd, MEMSIZE)) {
    perror("ftruncate");
    return (EXIT_FAILURE);
  }
  if (ftruncate(fdsem, sizeof(sem_t))) {
    perror("ftruncate");
    return (EXIT_FAILURE);
  }

  memory = (int*)mmap(0, MEMSIZE, PROT_WRITE | PROT_READ, MAP_SHARED, fd, 0);
  memsem = (sem_t*)mmap(
    0, sizeof(sem_t), PROT_WRITE | PROT_READ, MAP_SHARED, fdsem, 0);
  if (-1 == close(fd) || -1 == close(fdsem)) {
    perror("close");
  }
  if (sem_init(memsem, 1, 1) == -1) {
    perror("sem_init");
    return (EXIT_FAILURE);
  }

  sem_wait(memsem);
  whodafuqami = fork();
  if (whodafuqami > 0) {
    /* Parent */
    for (i = 0; i < ARRSIZE; i++) {
      memory[i] = (i + 1);
    }
    sem_post(memsem);

  } else if (whodafuqami == 0) {
    /* Child */
    sem_wait(memsem);
    for (i = 0; i < ARRSIZE; i++) {
      printf("%d\n", memory[i]);
    }
    sem_post(memsem);
    sem_destroy(memsem);
  } else { /* Error */
    perror("fork");
  }

  return (EXIT_SUCCESS);
}
