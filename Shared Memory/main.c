#include <inttypes.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>

#define SHARED_MEM "/sharedLuv"

int
main(void)
{
  uint8_t* data = NULL;
  uint32_t* pdata = NULL;
  sem_t* sema = NULL;
  data =
    mmap(NULL, 4096, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);

  if (data == MAP_FAILED) {
    perror("mmap");
    exit(1);
  }

  memset(data, 0xfb, 10);
  sema = (sem_t*)(data + 10);

  if (sem_init(sema, 1, 1) < 0) {
    perror("sem_init");
    exit(1);
  }
  sem_wait(sema);
  switch (fork()) {
    case 0: { // Child
      sem_wait(sema);
      pdata = (uint32_t*)(data + 10 + sizeof(sem_t));
      for (size_t i = 0; i < (4096 - 10 - sizeof(sem_t)) / 4; i++) {
        printf("%d ", (int)pdata[i]);
      }
      printf("\n");
      sem_post(sema);
      break;
    }
    case -1: { // Error
      break;
    }
    default: { // Parent
      pdata = (uint32_t*)(data + 10 + sizeof(sem_t));
      for (size_t i = 0; i < (4096 - 10 - sizeof(sem_t)) / 4; i++) {
        pdata[i] = i + 1;
      }
      sem_post(sema);
      break;
    }
  }

  return 0;
}
