#include <functional>
#include <iostream>

class Clasa
{
public:
  int suma(int a, int b, int c)
  {
    std::cout << a << " " << b << " " << c << std::endl;
    return (a + b + c);
  }
};

int
suma2(int a, int b, int c)
{
  std::cout << a << " " << b << " " << c << std::endl;
  return (a + b + c);
}

void
call(std::function<int(int, int)> func)
{
  func(1, 2);
}

int
main(void)
{
  auto a = std::bind(suma2, std::placeholders::_1, std::placeholders::_2,
                     std::placeholders::_3);
  auto b = std::bind(suma2, std::placeholders::_3, std::placeholders::_1,
                     std::placeholders::_2);
  auto c = std::bind(suma2, std::placeholders::_1, 10, std::placeholders::_3);
  auto d = std::bind(&Clasa::suma, Clasa(), std::placeholders::_1,
                     std::placeholders::_2, std::placeholders::_3);
  a(10, 11, 12);
  b(10, 11, 12);
  c(10, 11, 12);
  d(10, 11, 12);
  call(std::bind(suma2,std::placeholders::_1,std::placeholders::_2,0));

  return (EXIT_SUCCESS);
}
